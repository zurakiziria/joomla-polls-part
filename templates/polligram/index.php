<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$user = JFactory::getUser();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Output as HTML5
$doc->setHtml5(true);

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');

if ($task == "edit" || $layout == "form") {
    $fullWidth = 1;
} else {
    $fullWidth = 0;
}

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');

$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/js/jquery-ui.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/js/template.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/js/jquery.nicescroll.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/js/jquery.bxslider.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/js/foriframe.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/js/custom.js');
$doc->addScriptVersion('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/js/BsMultiSelect.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/js/parallax.min.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/js/jquery.datetimepicker.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/js/jquery.datetimepicker.min.js');

// Add Stylesheets
//$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/css/template.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/css/bootstrap.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/css/font-awesome/css/font-awesome.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/css/jquery.bxslider.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/css/style.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/css/jquery.datetimepicker.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/css/BsMultiSelect.css');
// Use of Google Font
if ($this->params->get('googleFont')) {
    $doc->addStyleSheet('//fonts.googleapis.com/css?family=' . $this->params->get('googleFontName'));
    $doc->addStyleDeclaration("
	h1, h2, h3, h4, h5, h6, .site-title {
		font-family: '" . str_replace('+', ' ', $this->params->get('googleFontName')) . "', sans-serif;
	}");
}

// Template color
if ($this->params->get('templateColor')) {
    /* $doc->addStyleDeclaration("
      body.site {
      border-top: 3px solid " . $this->params->get('templateColor') . ";
      background-color: " . $this->params->get('templateBackgroundColor') . ";
      }
      a {
      color: " . $this->params->get('templateColor') . ";
      }
      .nav-list > .active > a,
      .nav-list > .active > a:hover,
      .dropdown-menu li > a:hover,
      .dropdown-menu .active > a,
      .dropdown-menu .active > a:hover,
      .nav-pills > .active > a,
      .nav-pills > .active > a:hover,
      .btn-primary {
      background: " . $this->params->get('templateColor') . ";
      }"); */
}

// Check for a custom CSS file
$userCss = JPATH_SITE . '/templates/' . $this->template . '/css/user.css';

if (file_exists($userCss) && filesize($userCss) > 0) {
    $this->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/css/user.css');
}

// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Adjusting content width
if ($this->countModules('position-7') && $this->countModules('position-8')) {
    $span = "col-md-6";
} elseif ($this->countModules('position-7') && !$this->countModules('position-8')) {
    $span = "col-md-9";
} elseif (!$this->countModules('position-7') && $this->countModules('position-8')) {
    $span = "col-md-9";
} else {
    $span = "col-md-12";
}

// Logo file or site title param
if ($this->params->get('logoFile')) {
    $logo = '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '" />';
} elseif ($this->params->get('sitetitle')) {
    $logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($this->params->get('sitetitle'), ENT_COMPAT, 'UTF-8') . '</span>';
} else {
    $logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
    <head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-163298689-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-163298689-1');
</script>
<!-- Global site tag (gtag.js) - Google Ads: 993965656 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-993965656"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-993965656');
</script>
<!-- Event snippet for Website traffic conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-993965656/UdzOCISxwc0BENjs-tkD'});
</script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <jdoc:include type="head" />
    <!--[if lt IE 9]><script src="<?php echo JUri::root(true); ?>/media/jui/js/html5.js"></script><![endif]-->
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5e7ddd061b93d200194e5e14&product=inline-share-buttons&cms=sop' async='async'></script>
   




    <style>
        .parallax-window {
            height: calc(100vh - 33px);
            width:100%;
            background: rgb(2,0,36);
            background: linear-gradient(0deg, rgba(0,0,0,0.5) 0%, rgba(0,0,0,0.6) 61%, rgba(0,0,0,0.9) 100%);
        }
    </style>
</head>

<body class="site <?php
echo $option
 . ' view-' . $view
 . ($layout ? ' layout-' . $layout : ' no-layout')
 . ($task ? ' task-' . $task : ' no-task')
 . ($itemid ? ' itemid-' . $itemid : '')
 . ($params->get('fluidContainer') ? ' fluid' : '');
echo ($this->direction == 'rtl' ? ' rtl' : '');
?>">
<!--    <script>
        fbq('track', 'FindLocation');
    </script>-->



    <div id="popupAlert" class="overlay" onclick="popupClose()" ></div>
    <!-- Body -->
    <div class="body">

        <!-- Header -->
        <header class="header" role="banner">
            <div class="header-inner clearfix">
                <div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
                    <div class="row">
                        <a class="brand pull-left col-lg-3 col-md-6 col-10" href="<?php echo $this->baseurl; ?>/">
                            <?php echo $logo; ?>
                            <?php if ($this->params->get('sitedescription')) : ?>
                                <?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription'), ENT_COMPAT, 'UTF-8') . '</div>'; ?>
                            <?php endif; ?>
                        </a>

                        <?php if ($this->countModules('position-1')) : ?>
                            <nav class="navigation col-lg-6 col-md-2 col-2" role="navigation">
                                <div class="nav-collapse">
                                    <div id="menu-close"><i class="fa fa-times" aria-hidden="true"></i></div>
                                    <jdoc:include type="modules" name="position-1" style="none" />
                                    <div class="mobile-category-button">Categories</div>
                                </div>

                                <div id="mobile-menu"><i class="fa fa-bars" aria-hidden="true"></i></div>
                            </nav>
                        <?php endif; ?>
                        <div class="header-search col-lg-3 col-md-4 pull-right">
                            <jdoc:include type="modules" name="position-0" style="none" />
                        </div>

                    </div>
                </div>
            </div>
        </header>
        <?php if ($this->countModules('position-1')) : ?>
         <jdoc:include type="modules" name="parallax" style="none" />
         <?php endif; ?>
        <?php if ($this->countModules('position-1')) : ?>
            <div class="category-menu">
                <div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
                    <jdoc:include type="modules" name="categorymenu" style="none" />
                </div>
            </div>
        <?php endif; ?>

        <?php if ($this->countModules('poll-module')) : ?>
            <div class="poll-module">
                <div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
                    
                    <jdoc:include type="modules" name="poll-module" style="xhtml" />
                </div>
            </div>
        <?php endif; ?>

        <jdoc:include type="modules" name="banner" style="xhtml" />
        <div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
            <div class="row-fluid">
                <?php if ($this->countModules('position-8')) : ?>
                    <!-- Begin Sidebar -->
                    <div id="sidebar" class="col-md-3">
                        <div class="sidebar-nav">
                            <jdoc:include type="modules" name="position-8" style="xhtml" />
                        </div>
                    </div>
                    <!-- End Sidebar -->
                <?php endif; ?>
                <main id="content" role="main" class="<?php echo $span; ?>">
                    <!-- Begin Content -->
                    <jdoc:include type="modules" name="position-3" style="xhtml" />
                    <jdoc:include type="message" />
                    <jdoc:include type="component" />
                    <jdoc:include type="modules" name="position-2" style="none" />
                    <!-- End Content -->
                </main>
                <?php if ($this->countModules('position-7')) : ?>
                    <div id="aside" class="col-md-3">
                        <!-- Begin Right Sidebar -->
                        <jdoc:include type="modules" name="position-7" style="well" />
                        <!-- End Right Sidebar -->
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <!-- Footer -->
    <footer class="footer" role="contentinfo">
        <div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
            <jdoc:include type="modules" name="footer" style="none" />
            <a href="https://www.tagdirectory.info/4844/seo/agile">Agile SEO</a>
            <div class="pull-right">
                <span class="footer-text"> BY <b>TILK </b>GROUP </span>
            </div>
            <div>
                <span class="footer-text"> &copy; <?php echo "2019"; ?> <?php echo 'Polligram.net'; ?> </span>
            </div>
        </div>
    </footer>
<jdoc:include type="modules" name="debug" style="none" />
</body>
</html>
