<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  mod_popular
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_content/models', 'ContentModel');

/**
 * Helper for mod_popular
 *
 * @since  1.6
 */
abstract class ModRelatedPollsHelper {

    /**
     * Get a list of the most popular articles
     *
     * @param   JObject  &$params  The module parameters.
     *
     * @return  array
     */
    public static function getPolls(&$params) {

        $num = $params->get('count', 4);
        $order = $params->get('order', 'DESC');
        $sort = $params->get('sort', 'a.id');


        $db = JFactory::getDbo();
        $id = JRequest::getInt('id', 0);
        if (!$id) {
            return false;
        }
        $query = 'SELECT * FROM #__polls WHERE id = ' . $id;
        $db->setQuery($query);
        $poll = $db->loadObject();

        if (!isset($poll->id)) {
            return false;
        }

        $query = $db->getQuery(true);
        $query->select('a.*,(SELECT COUNT(*) FROM #__polls_vote WHERE poll_id = a.id AND state = 1) as votes')
                ->from('#__polls AS a');

        // Join over the language
        $query->select('l.title AS language_title')
                ->join('LEFT', $db->quoteName('#__languages') . ' AS l ON l.lang_code = a.language');

        $query->select('uc.name AS editor')
                ->join('LEFT', '#__users AS uc ON uc.id = a.checked_out');



        // Join over the users for the author.
        $query->select('ua.name AS author_name')
                ->join('LEFT', '#__users AS ua ON ua.id = a.created_by');

        // Join over the users for the modifier.
        $query->select('um.name AS modifier_name')
                ->join('LEFT', '#__users AS um ON um.id = a.modified_by');
        // Join over the users for the modifier.


        
        // Filter on the language.
        //   if ($language = $this->getState('filter.language')) {
        $query->where('a.language IN (' . $db->quote(JFactory::getLanguage()->getTag()) . ' ,' . $db->quote("*") . ' )');
        //  }

        $nullDate = $db->quote($db->getNullDate());
        $nowDate = $db->quote(JFactory::getDate()->toSql());

        if ($poll->catid) {
            $registry = new JRegistry;
            $registry->loadString($poll->catid);
            $catid = implode(',', $registry->toArray());

            $query->where('a.id IN (SELECT poll_id FROM #__category_rel WHERE cat_id in ( ' . $catid . ')) AND a.id != '.$id);
        }
        $query->where('a.publish_down > ' . $nowDate . ' OR a.publish_down = ' . $nullDate);
        $query->where('a.state = ' . (int) 1);
        
        // Filter by start and end dates.
        // $query->where('(a.publish_up = ' . $nullDate . ' OR a.publish_up <= ' . $nowDate . ')')
        //         ->where('(a.publish_down = ' . $nullDate . ' OR a.publish_down >= ' . $nowDate . ')');


        $query->order($db->escape($sort) . ' ' . $db->escape($order));
        $query->setLimit($num);
        $db->setQuery($query);
        $result = $db->loadObjectList();

        return $result;
    }

}
