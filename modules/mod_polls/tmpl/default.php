<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  mod_popular
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
jimport('tilklib.helper.helper');
jimport('tilklib.helper.pollhelper');
jimport('tilklib.image.img');
$Itemid = tilkHelper::getItemid('com_polls', 'poll');
$content_params = JComponentHelper::getParams('com_polls');
$w = $content_params->get('listing_image_width', 500);
$h = $content_params->get('listing_image_height', 400);
$wf = $content_params->get('first_width', 700);
$hf = $content_params->get('first_height', 500);
$first_view = $params->get('first_view', 0);
$firt_count = $params->get('firt_count', 5);
$doc = JFactory::getDocument();
JHtml::_('bootstrap.tooltip');
$prefix = $params->get('moduleclass_sfx', '');
$prefixh = $params->get('header_class', '');
?>

<div class="module-polls-main <?php echo $prefixh; ?>">
    <?php
    if ($list) {
        $polls = $list;
        $arrayROW = array();
        $i = 0;
        foreach ($list as $row) {

            $date = $row->publish_down;
            $jdate = JFactory::getDate($date);

            if (strtotime($jdate->format('Y-m-d H:i:s', true)) > time()) {
                $arrayROW[$i]['time'] = strtotime($jdate->format('Y-m-d H:i:s', true)) - time();
                $arrayROW[$i]['id'] = $row->id;
                $i++;
            }
        }

        $arrayROWJS = '';
        if ($arrayROW) {
            $arrayROWJS = json_encode($arrayROW);
        }
        $js = "
                        jQuery(document).ready(function(){
                        addDate" . $prefixh . "(" . $arrayROWJS . ")
                    });

                    function addDate" . $prefixh . "(dateArray){
                       if(dateArray){
                          dateArray.forEach(function(entry) {
                             
                             console.log(entry);
                             setInterval(function () {
                             timer = entry.time;
                             day = parseInt(entry.time / 60 / 60 / 24, 10);
                             hour = parseInt((entry.time - day*24*60*60)  / 60 / 60, 10);
                             minutes = parseInt((entry.time - day*24*60*60 - hour*60*60) / 60, 10);
                             seconds = parseInt(entry.time % 60, 10);

                             day = day < 10 ? '0' + day : day;
                             hour = hour < 10 ? '0' + hour : hour;
                             minutes = minutes < 10 ? '0' + minutes : minutes;
                             seconds = seconds < 10 ? '0' + seconds : seconds;
        
                             day_var = '';
                             hour_var = '';
                             minutes_var = '';
                             seconds_var = '';
        
                             day_var = day + ' " . JText::_('Day') . " ';
                             hour_var = ' '+hour+':';
                             minutes_var = minutes+':';
                             seconds_var = seconds;
                             jQuery('#date_'+entry.id+'" . $prefixh . "').html(day_var + ' ' + hour_var + minutes_var + seconds_var);
                             if (--entry.time < 0) {
                                 entry.time = 0;
                             }
                            }, 1000);
                           });
                       }
                    }
                    ";
        $doc->addScriptDeclaration($js);
        ?>
        <div class="polls row">
            <?php
            $i = 0;
            foreach ($polls as $poll) {
                $link = JRoute::_('index.php?option=com_polls&view=poll&id=' . $poll->id . '&Itemid=' . $Itemid);
                if (!$i && $first_view) {
                    ?>
                        <div class="col-md-12 mb-5 first-poll">
                            <div class="popular-poll">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="popular-poll-image">
                                            <?php
                                            if ($poll->image1) {

                                                $resize = new Image;
                                                $img = $resize->crop($poll->image1, $w, $h);
                                                ?>
                                                <img src="<?php echo JUri::root() . $img; ?>" alt="<?php echo $poll->title; ?>" />
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6 d-flex flex-column justify-content-between">
                                        <div>
                                            <h2 class="popular-poll-title">
                                                <a href="<?php echo tilkHelper::getPollLink($poll->id, $poll->title); ?>" > <?php echo $poll->title; ?> </a>
                                            </h2>
                                            <div class="popular-poll-answers">
                                                <?php
                                                $answers = pollHelper::getPollAnswers($poll->id, 2, $firt_count);
                                                if ($answers) {
                                                    ?>
                                                    <!--sorted by top-->
                                                    <div class="popular-poll-answers-items">
                                                        <?php
                                                        foreach ($answers as $ans) {
                                                            ?>
                                                            <div class="popular-poll-answers-item">
                                                                <b><?php echo $ans->title; ?></b>
                                                                <div class="popular-poll-answers-item-bar" style="width:<?php echo $ans->percent; ?>%;"><span><?php echo $ans->percent; ?>%</span></div>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="d-flex justify-content-between">
                                                <div>
                                                    <a href="<?php echo tilkHelper::getPollLink($poll->id, $poll->title); ?>" class="popular-poll-button">Take Part</a>
                                                </div>
                                                <div class="popular-poll-end-date">
                                                    <?php
                                                    $date = $poll->publish_down;
                                                    $jdate = JFactory::getDate($date);
                                                    if (strtotime($jdate->format('Y-m-d H:i:s', true)) > time()) {
                                                        ?>

                                                        <div class="countdown" id="date_<?php echo $poll->id . $prefixh; ?>"></div>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <div class="countdown"><?php echo JText::_('Ended'); ?></div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                } else {
                    ?>

                    <div class='poll-block col-md-3' id='pollvote'>
                        <div class="poll-block-inner">

                            <div class='poll-block-mainimage-tolist'>
                                <?php
                                if ($poll->image1) {

                                    $resize = new Image;
                                    $img = $resize->crop($poll->image1, $w, $h);
                                    ?>
                                    <a href="<?php echo tilkHelper::getPollLink($poll->id, $poll->title); ?>"> <img src='<?php echo $img; ?>' title='<?php echo $poll->title; ?>' alt='poll image'> </a>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="poll-block-info">
                                <?php
                                $date = $poll->publish_down;
                                $jdate = JFactory::getDate($date);
                                if (strtotime($jdate->format('Y-m-d H:i:s', true)) > time()) {
                                    ?>

                                    <div class="countdown" id="date_<?php echo $poll->id . $prefixh; ?>"></div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="countdown"><?php echo JText::_('Ended'); ?></div>
                                    <?php
                                }
                                ?>
                                <div class='poll-block-title'><a href="<?php echo tilkHelper::getPollLink($poll->id, $poll->title); ?>" title="<?php echo $poll->title; ?>"><?php echo $poll->title; ?></a></div>
                                <?php
                                $ans = pollHelper::getPollLiderAnswer($poll->id);
                                if ($ans) {
                                    ?>
                                    <div class='poll-block-lideranswer'>
                                        <?php ?>
                                        <div class='poll-block-lideranswers-block d-flex justify-content-between'>
                                            <div class='poll-block-lideranswers-list'><i class="fa fa-star" aria-hidden="true" title="Leader"></i> <?php echo $ans->title; ?></div>
                                            <div class='poll-block-vote-list'><?php echo $ans->votes; ?> <i class="fa fa-thumbs-up" aria-hidden="true"></i></div>
                                        </div>
                                        <?php ?>
                                    </div>
                                    <?php
                                } else {
                                    echo JText::_('WILL_BE_SOON');
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                    <?php
                }
                $i++;
            }
            ?>

        </div>

        <?php
    }
    ?>
</div>