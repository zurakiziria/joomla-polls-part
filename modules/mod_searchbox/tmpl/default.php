<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  mod_popular
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
jimport('tilklib.helper.helper');
jimport('tilklib.helper.pollhelper');
jimport('tilklib.image.img');
$Itemid = tilkHelper::getItemid('com_polls', 'poll');
$content_params = JComponentHelper::getParams('com_polls');
$w = $content_params->get('listing_image_width', 500);
$h = $content_params->get('listing_image_height', 400);
$doc = JFactory::getDocument();
JHtml::_('bootstrap.tooltip');
$prefix = $params->get('moduleclass_sfx', '');
$js = 'jQuery(document).ready(function(){ '
        . '';
$js .= "
var start = 0; 
            jQuery('#searchbox').keyup(function() {
             
  var sln = jQuery(this).val().length;
  var value = jQuery(this).val();
  console.log(value);
  if (sln > 2){

jQuery.ajax({
type: 'POST',
        url: '?option=com_polls&task=polls.searchbox',
        data: {val: value, sort: '".$sort."', limit: '".$num."', order: '".$order."', type: '".$type."'},
        dataType: 'json',
        success: function (data) {

        jQuery('.searchboxlist').html(data.msg);
               
        }
}
);

  }else{
  jQuery('.searchboxlist').html('');
}
  
});

});
";
//if ($getpag) {
$doc->addScriptDeclaration($js);
?>

<div class="module-polls<?php echo $prefix; ?>">
    <div class="mainsearchinput">
        <input type="text" placeholder="<?php echo JText::_('Search_Poll'); ?>" name="searchbox" id="searchbox" >
        <i class="module-polls-search"></i>
    </div>
    <div class="searchboxlist"></div>
</div>