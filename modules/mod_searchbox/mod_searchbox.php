<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  mod_popular
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the mod_popular functions only once.
JLoader::register('ModSearchboxHelper', __DIR__ . '/helper.php');

// Get module data.
//$list = ModPollsHelper::getPolls($params);


       
        $type = $params->get('type', 0);
        $num = $params->get('count', 4);
        $order = $params->get('order', 'DESC');
        $sort = $params->get('sort', 'a.id');

// Render the module
require JModuleHelper::getLayoutPath('mod_searchbox', $params->get('layout', 'default'));
