<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_fbsharetags
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;


$text = $params->get('desc');
$title = $params->get('title');
$main_image = $params->get('image');





$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'), ENT_COMPAT, 'UTF-8');

require JModuleHelper::getLayoutPath('mod_fbsharetags', $params->get('layout', 'default'));
