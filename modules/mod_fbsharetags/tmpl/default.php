<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_fbsharetags
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
$document = JFactory::getDocument();
$art = JRequest::getVar('article',0);
jimport('tilklib.helper.helper');
if (!$art){
if ($title){
    $document->setTitle($title);
    $document->addCustomTag('<meta property="og:title" content="' . tilkHelper::word_limit($title, 55, 1) . '" />');
}
$uri = JUri::getInstance(); 

    $document->addCustomTag('<meta property="og:url" content="'.$uri->toString().'" />');
    if ($main_image) {

        $document->addCustomTag('<meta property="og:image" content="' . JURI::root() . $main_image . '" />');
//        $document->addCustomTag(' <meta property="og:image:width" content="' . $share_w . '" />');
//        $document->addCustomTag('<meta property="og:image:height" content="' . $share_h . '" />');
        $document->addCustomTag('<link rel="image_src" href="' . JURI::root() . $main_image . '" / >');
    }

    if ($text) {
        $document->addCustomTag('<meta property="og:description" content="' . tilkHelper::word_limit(str_replace("&nbsp;", "", ($text)), 180, 1) . '">');
        $document->setDescription($text);
    }
}


?>
