<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  mod_nextpoll
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
jimport('tilklib.helper.helper');
$Itemid = tilkHelper::getItemid('com_polls', 'poll');
$link = tilkHelper::getPollLink($pl->id, $pl->title);
?>

<div class="mod-nextpoll">
    <div class="mod-nextpoll-clicker-block">
        <a href="<?php echo $link; ?>">>></a>
    </div>
</div>