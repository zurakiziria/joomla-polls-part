<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  mod_popular
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

//JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_content/models', 'ContentModel');

/**
 * Helper for mod_popular
 *
 * @since  1.6
 */
abstract class ModNextpollHelper {

    /**
     * Get a list of the most popular articles
     *
     * @param   JObject  &$params  The module parameters.
     *
     * @return  array
     */
    public static function getNextPoll($params) {

        $db = JFactory::getDBO();
        $id = JRequest::getInt('id', 0);
        $ip = $db->quote($_SERVER['REMOTE_ADDR']);
        $br = $db->quote($_SERVER['HTTP_USER_AGENT']);
        $timezone = new DateTimeZone('Asia/Tbilisi');
        $date = JFactory::getDate()->format('Y-m-d h:i:s', $timezone);
        $content_params = JComponentHelper::getParams('com_polls');
        $way = $content_params->get('vote_way', 1);
        if (!$id) {
            return false;
        }

        $query = 'SELECT id,vote_way,title FROM #__polls WHERE id < ' . $id . ' AND state = 1 AND publish_down >= "' . $date . '"  '
                . ' '
                . 'ORDER BY id DESC';
        $db->setQuery($query);
        $result = $db->loadObject();


        if (!isset($result->id)) {
            $query = 'SELECT id,vote_way,title FROM #__polls WHERE id > ' . $id . ' AND state = 1 AND publish_down >= "' . $date . '"'
                    . ' '
                    . ' ORDER BY id DESC';
            $db->setQuery($query);
            $result = $db->loadObject();
        }

        if (!isset($result->id)) {
            return false;
        }


        return $result;
    }

}
