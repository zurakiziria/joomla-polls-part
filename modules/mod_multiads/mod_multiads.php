<?php

/**
 * @package     Joomla.Site
 * @subpackage  mod_footer
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

$app = JFactory::getApplication();
$date = JFactory::getDate();

$ads = array();
$ads_percent = array();

$ads[1]['img'] = $params->get('ad1');
$ads[1]['iframe'] = $params->get('ad1_iframe');
$ads[2]['img'] = $params->get('ad2');
$ads[2]['iframe'] = $params->get('ad2_iframe');
$ads[3]['img'] = $params->get('ad3');
$ads[3]['iframe'] = $params->get('ad3_iframe');
$ads[4]['img'] = $params->get('ad4');
$ads[4]['iframe'] = $params->get('ad4_iframe');
$ads[5]['img'] = $params->get('ad5');
$ads[5]['iframe'] = $params->get('ad5_iframe');

$ads_percent[1] = $params->get('ad1_percent');
$ads_percent[2] = $params->get('ad2_percent');
$ads_percent[3] = $params->get('ad3_percent');
$ads_percent[4] = $params->get('ad4_percent');
$ads_percent[5] = $params->get('ad5_percent');

if ($ads) {

    $ads_per = array();
    $ads_per[0] = array();
    $ads_per[0]['per'] = 0;
    $ads_per[0]['id'] = 0;
    $prev = 0;
    $good_k = 0;

    $i = 1;
    foreach ($ads as $k => $r) {
        if (empty($r['img']) && empty($r['iframe'])) {
            unset($ads[$k]);
            unset($ads_percent[$k]);
        } else {
            $ads_per[$i] = array();
            $ads_per[$i]['per'] = $prev + $ads_percent[$k];
            $ads_per[$i]['id'] = $k;
            $prev = $ads_per[$i]['per'];
            $i++;
            $good_k = $k;
        }
    }

    if (count($ads) == 1) {
        $ad_id = $good_k;
    } else {
        $rand = rand(0, 100);

        $i = 0;
        $ad_id = 0;
        for ($i = 1; $i < count($ads_per); $i++) {
            $prev = $ads_per[$i - 1]['per'];
            $curr = $ads_per[$i]['per'];

            $curr_id = $ads_per[$i]['id'];
            if ($prev < $rand && $rand < $curr) {
                $ad_id = $curr_id;
                break;
            }
        }
    }


    if ($ad_id != 0) {
        
        if(!empty($ads[$ad_id]['img'])){
            $ad = $ads[$ad_id]['img'];
            $ad_type = 1;
        }
        else{
            $ad = $ads[$ad_id]['iframe'];
             $ad_type = 0;
        }
        
        $layout = $params->get('type',  'default');
        
        if($layout == 'popup'){
            
        }
        
        $moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'), ENT_COMPAT, 'UTF-8');
        $id = htmlspecialchars($params->get('id'), ENT_COMPAT, 'UTF-8');

        require JModuleHelper::getLayoutPath('mod_multiads', $layout);
    }
}


