<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_footer
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
?>
<div class="ads_<?php echo $moduleclass_sfx ?>">
    <div class="ads_in_<?php echo $moduleclass_sfx ?>" id="<?php echo $id; ?>">
        <?php
        if ($ad_type) {
            ?>
            <img src="<?php echo $ad ?>" />
            <?php
        } else {
            ?>
            <iframe src="<?php echo $ad ?>" ></iframe>
            <?php
        }
        ?>
    </div>
</div>
