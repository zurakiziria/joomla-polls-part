<?php

class Tilkstring {

    protected static $_alphabet = array(
        'ა' => 'a',
        'ბ' => 'b',
        'გ' => 'g',
        'დ' => 'd',
        'ე' => 'e',
        'ვ' => 'v',
        'ზ' => 'z',
        'თ' => 't',
        'ი' => 'i',
        'კ' => 'k',
        'ლ' => 'l',
        'მ' => 'm',
        'ნ' => 'n',
        'ო' => 'o',
        'პ' => 'p\'',
        'ჟ' => 'zh',
        'რ' => 'r',
        'ს' => 's',
        'ტ' => 't\'',
        'უ' => 'u',
        'ფ' => 'p',
        'ქ' => 'k',
        'ღ' => 'gh',
        'ყ' => 'q\'',
        'შ' => 'sh',
        'ჩ' => 'ch',
        'ც' => 'ts',
        'ძ' => 'dz',
        'წ' => 'ts\'',
        'ჭ' => 'ch\'',
        'ხ' => 'kh',
        'ჯ' => 'j',
        'ჰ' => 'h',
    );

    public static function katoen($text) {

        $text = str_replace(
                array_keys(self::$_alphabet), array_values(self::$_alphabet), preg_replace_callback(
                        // make capital from first chars of sentences
                        '/(^|[\.\?\!]\s*)([a-z])/s', function ($m) {
                    return $m[1] . strtoupper($m[2]);
                }, $text)
        );
        return $text;
    }

}
