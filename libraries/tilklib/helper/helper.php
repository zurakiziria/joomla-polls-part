<?php

abstract class tilkHelper {

    public static function getItemid($component, $view = null, $type = 'component') {

        if ($view) {
            $items = JFactory::getApplication()->getMenu()->getItems('component', $component);

            if ($items) {
                foreach ($items as $row) {
                    if (isset($row->query['view']) && $row->query['view'] == $view) {
                        return $row->id;
                    }
                }
            }
            if (isset($Itemid[0]->id)) {
                return $Itemid[0]->id;
            }
            return 0;
        } else {
            $Itemid = JFactory::getApplication()->getMenu()->getItems('component', $component, true)->id;
        }
        return $Itemid;
    }

    public static function word_limit($string, $limit = 0, $strip = 0, $link = '') {
        // $string = strip_tags($string);
        if ($strip) {
            $string = strip_tags($string);
        }
        
        if ($limit && mb_strlen($string) > $limit) {
            $href = '';
            if ($link) {
                $href = '<a href="' . $link . '">Read More</a>';
            }
            // truncate string
            $stringCut = mb_substr($string, 0, $limit);

            // make sure it ends in a word so assassinate doesn't become ass...
            $string = mb_substr($stringCut, 0, strrpos($stringCut, ' ')) . '... ' . $href;
            $string = str_replace('"', '',$string);
        }
        return $string;
    }

    public static function getPollLink($id, $title = '') {
        $Itemid = tilkHelper::getItemid('com_polls', 'poll');

        if ($title) {
            $ka_array = array('ა', 'ბ', 'გ', 'დ', 'ე', 'ვ', 'ზ', 'თ', 'ი', 'კ', 'ლ', 'მ', 'ნ', 'ო', 'პ', 'ჟ', 'რ', 'ს', 'ტ', 'უ', 'ფ', 'ქ', 'ღ', 'ყ', 'შ', 'ც', 'ჩ', 'ძ', 'წ', 'ჭ', 'ხ', 'ჯ', 'ჰ');

            $en_array = array('a', 'b', 'g', 'd', 'e', 'v', 'z', 't', 'i', 'k', 'l', 'm', 'n', 'o', 'p', 'j', 'r', 's', 't', 'u', 'f', 'q', 'g', 'y', 'sh', 'ts', 'ch', 'dz', 'w', 'ch', 'kh', 'j', 'h');

            $ru_array = array('а', 'г', 'г', 'д', 'е', 'в', 'з', 'т', 'и', 'к', 'л', 'м', 'н', 'о', 'п', 'ж', 'р', 'с', 'т', 'у', 'ф', '!@##@!', '!@##@!', '!@##@!', 'ш', 'ц', 'ч', '!@##@!', '!@##@!', '!@##@!', 'х', '!@##@!', '!@##@!');
            $dd_ru = array('ё', 'й', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
            $dd_en = array('e', 'i', 'sh', '', 'i', '', 'e', 'iu', 'ia');

            $url = preg_replace('~[^\\pL0-9_]+~u', '-', $title);
            $url = trim($url, "-");
            $url = strtolower($url);


            $url = str_replace($dd_ru, $dd_en, $url);
            $url = str_replace($ka_array, $en_array, $url);
            $url = str_replace($ru_array, $en_array, $url);

            $title = ':' . $url;
        }

        $url_id = $id . $title;

        
        $link = JRoute::_('index.php?option=com_polls&view=poll&id=' . $url_id . '&Itemid=' . $Itemid);
        return $link;
    }
    



    public static function shapeSpace_block_proxy_visits() {
	
	$headers = array('CLIENT_IP','FORWARDED','FORWARDED_FOR','FORWARDED_FOR_IP','VIA','X_FORWARDED','X_FORWARDED_FOR','HTTP_CLIENT_IP','HTTP_FORWARDED','HTTP_FORWARDED_FOR','HTTP_FORWARDED_FOR_IP','HTTP_PROXY_CONNECTION','HTTP_VIA','HTTP_X_FORWARDED','HTTP_X_FORWARDED_FOR','HTTP_X_NEWRELIC_ID','HTTP_X_NEWRELIC_TRANSACTION');

	foreach ($headers as $header){
		if (isset($_SERVER[$header])) {
			return true;
		}
	}
        return false;
}

}
