<?php

abstract class pollHelper {

    public static function getPollAnswers($id, $ord = 0, $limit = false, $is_user = false) {
        if (!$id) {
            return false;
        }
        $addsql = '';
        if ($limit) {
            $addsql .= ' LIMIT ' . $limit;
        }
        $db = JFactory::getDBO();
        $query = 'SELECT pl.*, (SELECT COUNT(*) FROM #__polls_vote WHERE answer_id = pl.id AND poll_id = pl.poll_id AND state = 1) as votes, (SELECT COUNT(*) FROM #__polls_vote WHERE poll_id = pl.poll_id AND state = 1) as poll_votes FROM #__polls_answers AS pl '
                . 'WHERE pl.poll_id = ' . $id . ' ';
        if (!$ord) {
            $query .= ' ORDER BY pl.ordering DESC ';
        } elseif ($ord == 1) {
            $query .= ' ORDER BY pl.ordering ASC ';
        } else {
            $query .= ' ORDER BY votes DESC ';
        }
        $query .= '' . $addsql;
        $db->setQuery($query);
        $result = $db->loadObjectList();

        if ($result) {
            foreach ($result as $key => $p) {


                $result[$key]->percent = $p->poll_votes == 0 || $p->votes == 0 ? 0 : round((100 / $p->poll_votes) * $p->votes);
            }
            return $result;
        }
    }

    public static function getPollLiderAnswer($id) {

        if (!$id) {
            return false;
        }
        $db = JFactory::getDBO();

        $query = "SELECT an.*, (SELECT COUNT(*) FROM #__polls_vote WHERE answer_id = an.id AND poll_id = an.poll_id AND state = 1) as votes FROM #__polls_answers AS an "
                . "WHERE an.poll_id = " . $id . " AND an.state=1 "
                . "ORDER BY votes DESC ";
        $db->setQuery($query);
        $res = $db->loadObject();

        if ($res) {
            return $res;
        }
    }

    public static function getPollAnswer($id) {
        if (!$id) {
            return false;
        }
        $db = JFactory::getDBO();
        $query = "SELECT an.*, (SELECT COUNT(*) FROM #__polls_vote WHERE answer_id = an.id AND poll_id = an.poll_id AND state = 1) as votes FROM #__polls_answers AS an "
                . "WHERE an.id = " . $id . " AND an.state=1 "
                . " ";
        $db->setQuery($query);
        $res = $db->loadObject();

        if (isset($res->id)) {
            return $res;
        }
        return false;
    }

    public static function isVote() {
        $db = JFactory::getDBO();
        $id = JRequest::getInt('id', 0);
        $ip = $db->quote($_SERVER['REMOTE_ADDR']);
        $br = $db->quote($_SERVER['HTTP_USER_AGENT']);
        $timezone = new DateTimeZone('Asia/Tbilisi');
        $date = JFactory::getDate()->format('Y-m-d h:i:s', $timezone);
        $content_params = JComponentHelper::getParams('com_polls');
        $way = $content_params->get('vote_way', 1);
        if (!$id) {
            return false;
        }
        $pollquery = "SELECT * FROM #__polls WHERE id=" . $id . " AND state =1 ";
        $db->setQuery($pollquery);
        $pol = $db->loadObject();

        if (isset($pol->publish_down) && strtotime($pol->publish_down) && strtotime($pol->publish_down) <= strtotime($date)) {
            return true;
        }
        if (isset($pol->vote_way) && $pol->vote_way) {
            if ($pol->vote_way == 1) {
                //ip
                $way = 1;
            } elseif ($pol->vote_way == 2) {
                //cookie
                $way = 2;
            } elseif ($pol->vote_way == 3) {
                //ip browser
                $way = 0;
            }
        }


        if ($way == 1) {
            $addsql = "`ip` = " . $ip . "";
        } elseif ($way == 3) {
            $addsql = "`ip` = " . $ip . " AND  `agent` = " . $br . "";
        } else {
            $addsql = "`ip` = " . $ip . " AND  `agent` = " . $br . "";
        }
        $select = "SELECT `id`,`answer_id` FROM `#__polls_vote` WHERE `poll_id` = " . (int) $id . " AND " . $addsql . " AND state = 1";
        $db->setQuery($select);
        $res = $db->loadObject();

        if (isset($res->answer_id) && $res->answer_id) {
            return $res->answer_id;
        }
        return false;
    }

    public static function getBlacklistByIp($ip) {
        if ($ip) {
            return false;
        }
    }

    public static function checkSpam($ip, $poll_id) {
        if ($ip && $poll_id) {
            $db = JFactory::getDBO();
            $content_params = JComponentHelper::getParams('com_polls');
            $spam = $content_params->get('spam_num', 10);
            $yesterday = date('Y-m-d', strtotime("-1 days"));
            $pollquery = 'SELECT COUNT(*) AS spam FROM #__polls_vote WHERE poll_id = ' . (int) $poll_id . ' AND state = 1 AND ip = ' . $db->quote($ip) . ' AND date > ' . $db->quote($yesterday) . '';
            $db->setQuery($pollquery);
            $pol = $db->loadObject();
            if (isset($pol->spam) && $pol->spam > $spam) {
                return true;
            }
            return false;
        }
        return true;
    }

    public static function getRelCategoryArray($poll_id){
        $db = JFactory::getDBO();

        if ($poll_id){
            $query = 'SELECT cat_id FROM #__category_rel WHERE poll_id = '.(int)$poll_id.' ';
            $db->setQuery($query);
            $items = $db->loadObjectList();
            $data = array();
            if ($items){
                foreach ($items as $row){
                    $data[] = $row->cat_id;
                }
            }
            return $data;
        }
        return false;
    }
}
