<?php

defined('_JEXEC') or die;

// Image Resize/Crop Class
// 2016.04.09
// Gaga Gelashvili

class Image {

    public function resize($filename, $width = 0, $height = 0, $enLarge = true) {
     
      
        if ($width <= 0 && $height <= 0) {
            return $filename;
        }

        require_once 'ImageResize.php';

        $ext = pathinfo($filename, PATHINFO_EXTENSION);
         
        if ($ext != 'jpeg' && $ext != 'jpg' && $ext != 'png' && $ext != 'gif' && $ext != 'JPEG' && $ext != 'JPG' && $ext != 'PNG') {
            return false;
        }
        jimport('joomla.filesystem.file');
        jimport('joomla.filesystem.folder');

        $path = JPATH_BASE."/pic/" . $width . "X" . $height;
        $pathm = "pic/" . $width . "X" . $height;
      
       
        if (!JFolder::exists($path)) {
            JFolder::create($path);
        }

        $image = md5($filename . '|' . $width . '|' . $height . '|RESIZE') . '.' . $ext;
        $imagePath = $pathm . '/' . $image;


        if (file_exists($imagePath)) {
            return $imagePath;
        }
        
        $imgRSZ = new ImageResize($filename);

      
        if ($width > 0 && $height == 0) {
            $imgRSZ->resizeToWidth($width, $enLarge);
            $imgRSZ->save($imagePath, $ext, 80);
        } else if ($width == 0 && $height > 0) {
            $imgRSZ->resizeToHeight($height, $enLarge);
            $imgRSZ->save($imagePath, $ext, 80);
        } else {
            $imgRSZ->resize($width, $height, $enLarge);
            $imgRSZ->save($imagePath, $ext, 80);
            
        }

        return $imagePath;
    }

    public function crop($filename, $width, $height, $pos = 'center') {

        require_once 'ImageResize.php';
 
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
         
        if ($ext != 'jpeg' && $ext != 'jpg' && $ext != 'png' && $ext != 'gif' && $ext != 'JPEG' && $ext != 'JPG' && $ext != 'PNG') {
            return false;
        }

        jimport('joomla.filesystem.file');
        jimport('joomla.filesystem.folder');
        $path = JPATH_BASE."/pic/" . $width . "X" . $height;
        $pathm = "pic/" . $width . "X" . $height;
      

        if (!JFolder::exists($path)) {
            JFolder::create($path);
        }

        $image = md5($filename . '|' . $width . '|' . $height . '|CROP') . '.' . $ext;
        $imagePath = $pathm . DIRECTORY_SEPARATOR . $image;

        if (file_exists($imagePath)) {
            return $imagePath;
        }
        
        $imgRSZ = new ImageResize($filename);

        if($pos == 'top'){
            $posCrop = 1;
        }
        else if($pos == 'bottom'){
            $posCrop = 3;
        }
        else if($pos == 'left'){
            $posCrop = 4;
        }
        else if($pos == 'right'){
            $posCrop = 5;
        }
        else{
            $posCrop = 2;
        }
        $enLarge = true;
            $imgRSZ->crop($width, $height, $enLarge, $posCrop);
            $imgRSZ->save($imagePath, $ext, 80);

        return $imagePath;
    }

}