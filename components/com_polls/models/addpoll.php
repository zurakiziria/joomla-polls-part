<?php

/**
 * @author		
 * @copyright	
 * @license		
 */
defined("_JEXEC") or die("Restricted access");

/**
 * Item Model for company.
 *
 * @package     Poll
 * @subpackage  Models
 */
class PollsModelAddPoll extends JModelAdmin {

    /**
     * @var        string    The prefix to use with controller messages.
     * @since   1.6
     */
    protected $text_prefix = 'COM_POLL';

    /**
     * The type alias for this content type.
     *
     * @var      string
     * @since    3.2
     */
    public $typeAlias = 'com_polls.addpoll';

    public function getTable($type = 'Poll', $prefix = 'PollsTable', $config = array()) {
        return JTable::getInstance($type, $prefix, $config);
    }

    public function getPoll() {


        $db = JFactory::getDbo();

        $id = JRequest::getInt('id', 0);

        if (!$id) {
            return $this->getTable('Poll');
        }
        $user = JFactory::getUser();
        $user_id = (isset($user->id)) ? $user->id : 0;

        $query = "SELECT * FROM `#__polls` WHERE `created_by` = " . $user_id . " AND `id` = '" . (int) $id . "'  ORDER BY `id` DESC";
        $db->setQuery($query);
        $items = $db->LoadObject();

        if (!$items) {
            return $this->getTable('Poll');
        }

        return $items;
    }

    public function getCategory() {


        $db = JFactory::getDbo();



        $query = "SELECT * FROM `#__categories` WHERE `published` = 1 AND `extension` = " . $db->quote('com_polls') . " ORDER BY `lft` DESC";
        $db->setQuery($query);
        $items = $db->LoadObjectList();



        return $items;
    }

    public function addPoll($data) {
        $db = JFactory::getDbo();
        $user = JFactory::getUser();
        $id = (int) $data['id'];
        if (!$user->id) {
            return false;
        }

        if (isset($data['image1']) && isset($data['title']) && isset($data['text']) && isset($data['category']) && isset($data['layout']) && isset($data['show_result']) && isset($data['publish_down']) && isset($data['state']) && isset($data['created_by']) && isset($data['use_type']) && isset($data['ord_by'])) {

            if (is_array($data['category'])) {
                $catid = $data['category'];
                $registry = new JRegistry;
                $registry->loadArray($catid);
                $data['category'] = (string) $registry;
            }
            if ($id) {
                //shemowmdes gamokitxva tu ekutvnis users
                $sql = 'SELECT id FROM #__polls WHERE id = '.(int)$id.' AND created_by = '.$user->id.' ';
                $db->setQuery($sql);
                $pl = $db->loadObject();
                if (isset($pl->id) && $pl->id){
                    $query = 'UPDATE #__polls SET '
                            . 'title='.$db->quote($data['title']).',text='.$db->quote($data['text']).','
                            . 'catid='.$db->quote($data['category']).',state='.$db->quote($data['state']).','
                            . 'layout='.$db->quote($data['layout']).',image1='.$db->quote($data['image1']).','
                            . 'show_result='.$db->quote($data['show_result']).',publish_down='.$db->quote($data['publish_down']).','
                            . 'ord_by='.$db->quote($data['ord_by']).',use_type='.$db->quote($data['use_type']).',created_by='.$db->quote($data['created_by']).' '
                            . 'WHERE id = '.$pl->id;
                    $db->setQuery($query);
                    $db->query();
                    return $pl->id;
                            
                }else{
                    return false;
                }
            } else {
                $query = "INSERT INTO #__polls (title,text,catid,state,layout,image1,show_result,created_by,publish_down,ord_by,use_type)VALUES(" . $db->quote($data['title']) . "," . $db->quote($data['text']) . "," . $db->quote($data['category']) . ",".$db->quote($data['state'])."," . $db->quote($data['layout']) . "," . $db->quote($data['image1']) . "," . $db->quote($data['show_result']) . "," . $db->quote($data['created_by']) . "," . $db->quote($data['publish_down']) . "," . $db->quote($data['ord_by']) . "," . $db->quote($data['use_type']) . ")";
                $db->setQuery($query);
                $db->query();
                $id = $db->insertid();
                return $id;
            }
        }

        return false;
    }
    
    
            public function addAnswers($data) {

        $db = JFactory::getDBO();
        if (!$data['title']) {
            return false;
        }
        if (isset($data['id']) && $data['id'] && isset($data['poll_id'])) {
            $sql = 'SELECT * FROM #__polls_answers WHERE poll_id = ' . (int) $data['poll_id'] . ' AND id = ' . (int) $data['id'] . ' AND state = 1 ';
            $db->setQuery($sql);
            $answer = $db->LoadObject();

            if (isset($answer->id)) {
                $image1 = $data['image1'] ? $data['image1'] : $answer->image1;
                $update = 'UPDATE #__polls_answers SET ordering = ' . (int) $data['ordering'] . ', title = ' . $db->quote($data['title']) . ', image1 = ' . $db->quote($image1) . ' WHERE poll_id = ' . (int) $data['poll_id'] . ' AND id = ' . $data['id'] . ' ';
                $db->setQuery($update);
                $db->execute();
            }
        }
        if (isset($data['id']) && $data['id'] == 0) {
            $query = 'INSERT INTO #__polls_answers (ordering,poll_id,title,image1,state,language)VALUES(' . (int) $data['ordering'] . ',' . $data['poll_id'] . ',' . $db->quote($data['title']) . ',' . $db->quote($data['image1']) . ',1,"*")';
            $db->setQuery($query);
            $db->execute();
        }
    }

    /**
     * Method for getting the form from the model.
     *
     * @param   array    $data      Data for the form.
     * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
     *
     * @return  mixed  A JForm object on success, false on failure
     */
    public function getForm($data = array(), $loadData = true) {
        JForm::addFormPath(JPATH_COMPONENT_ADMINISTRATOR . '/models/forms');
        JForm::addFieldPath(JPATH_COMPONENT_ADMINISTRATOR . '/models/fields');

        JForm::addRulePath(JPATH_COMPONENT_ADMINISTRATOR . '/models/rules');

        $options = array('control' => 'jform', 'load_data' => $loadData);
        $form = $this->loadForm($this->typeAlias, $this->name, $options);

        if (empty($form)) {
            return false;
        }

        return $form;
    }

    /**
     * Method to get the data that should be injected in the form.
     *
     * @return  array    The default data is an empty array.
     */
    protected function loadFormData() {
        $app = JFactory::getApplication();
        $data = $app->getUserState($this->option . '.edit.' . $this->name . '.data', array());

        if (empty($data)) {
            $data = $this->getItem();
        }

        return $data;
    }

}

?>