<?php

/**
 * @author		
 * @copyright	
 * @license		
 */
defined("_JEXEC") or die("Restricted access");

/**
 * List Model for companies.
 *
 * @package     Polls
 * @subpackage  Models
 */
class PollsModelCategory extends JModelList {

    /**
     * Constructor.
     *
     * @param   array  $config  An optional associative array of configuration settings.
     */
    public function __construct($config = array()) {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'a.id', 'id',
                'a.alias', 'alias',
                'a.checked_out', 'checked_out',
                'a.checked_out_time', 'checked_out_time',
                'a.state', 'state',
                'a.access', 'access', 'access_level',
                'a.created', 'created',
                'a.created_by', 'created_by', 'author_id',
                'a.created_by_alias', 'created_by_alias',
                'a.ordering', 'ordering',
                'a.language', 'language',
                'a.hits', 'hits',
            );
        }
        parent::__construct($config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * This method should only be called once per instantiation and is designed
     * to be called on the first call to the getState() method unless the model
     * configuration flag to ignore the request is set.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @param   string  $ordering   An optional ordering field.
     * @param   string  $direction  An optional direction (asc|desc).
     *
     * @return  void
     */
    protected function populateState($ordering = 'id', $direction = 'ASC') {
        // Get the Application
        $app = JFactory::getApplication();
        $menu = $app->getMenu();

        // Set filter state for search
        $search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);

        // Set filter state for access
        $accessId = $this->getUserStateFromRequest($this->context . '.filter.access', 'filter_access', null, 'int');
        $this->setState('filter.access', $accessId);

        // Set filter state for author
        $authorId = $app->getUserStateFromRequest($this->context . '.filter.author_id', 'filter_author_id');
        $this->setState('filter.author_id', $authorId);

        // Set filter state for publish state
        $published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_state', '', 'string');
        $this->setState('filter.published', $published);

        // Set filter state for language
        $language = $this->getUserStateFromRequest($this->context . '.filter.language', 'filter_language', '');
        $this->setState('filter.language', $language);

        $Id = $app->input->getInt('id');
        $this->setState('filter.id', $Id);
        // force a language
        $forcedLanguage = $app->input->get('forcedLanguage');

        // Set filter state for language if a language is forced
        if (!empty($forcedLanguage)) {
            $this->setState('filter.language', $forcedLanguage);
            $this->setState('filter.forcedLanguage', $forcedLanguage);
        }

        // Load the parameters.
        $params = JComponentHelper::getParams('com_polls');
        $active = $menu->getActive();
        empty($active) ? null : $params->merge($active->params);
        $this->setState('params', $params);

        // List state information.
        parent::populateState($ordering, $direction);
    }

    /**
     * Method to get a store id based on model configuration state.
     *
     * This is necessary because the model is used by the component and
     * different modules that might need different sets of data or different
     * ordering requirements.
     *
     * @param   string  $id  A prefix for the store id.
     *
     * @return  string  A store id.
     *
     * @since   1.6
     */
    protected function getStoreId($id = '') {
        // Compile the store id.
        $id .= ':' . $this->getState('filter.search');
        $id .= ':' . $this->getState('filter.access');
        $id .= ':' . $this->getState('filter.state');

        $id .= ':' . $this->getState('filter.language');

        return parent::getStoreId($id);
    }

    /**
     * Build an SQL query to load the list data.
     *
     * @return  JDatabaseQuery
     */
    protected function getListQuery() {
        // Get database object
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select('a.*')->from('#__polls AS a');

        // Join over the language
        $query->select('l.title AS language_title')
                ->join('LEFT', $db->quoteName('#__languages') . ' AS l ON l.lang_code = a.language');

        $query->select('uc.name AS editor')
                ->join('LEFT', '#__users AS uc ON uc.id = a.checked_out');




        // Join over the users for the author.
        $query->select('ua.name AS author_name')
                ->join('LEFT', '#__users AS ua ON ua.id = a.created_by');

        // Join over the users for the modifier.
        $query->select('um.name AS modifier_name')
                ->join('LEFT', '#__users AS um ON um.id = a.modified_by');
        // Join over the users for the modifier.
        // Filter by search
        $search = $this->getState('filter.search');
        $s = $db->quote('%' . $db->escape($search, true) . '%');

        if (!empty($search)) {
            if (stripos($search, 'id:') === 0) {
                $query->where('a.id = ' . (int) substr($search, strlen('id:')));
            } elseif (stripos($search, 'id:') === 0) {
                $search = $db->quote('%' . $db->escape(substr($search, strlen('id:')), true) . '%');
                $query->where('(a.id LIKE ' . $search);
            } elseif (stripos($search, 'author:') === 0) {
                $search = $db->quote('%' . $db->escape(substr($search, 7), true) . '%');
                $query->where('(ua.name LIKE ' . $search . ' OR ua.username LIKE ' . $search . ')');
            } else {
                $search = $db->quote('%' . $db->escape($search, true) . '%');
            }
        }

        // Filter by published state.
//        $published = $this->getState('filter.published');
//        if (is_numeric($published)) {
//            $query->where('a.published = ' . (int) $published);
//        } elseif ($published === '') {
//            // Only show items with state 'published' / 'unpublished'
//            $query->where('(a.published IN (0, 1))');
//        }
        $query->where('a.state = ' . (int) 1);
        $catid = $this->getState('filter.id');
        
        if ($catid) {
            $query->where('a.id IN(SELECT poll_id FROM #__category_rel WHERE cat_id = ' . (int) $catid.')');
        }
        // Filter on the language.
        //   if ($language = $this->getState('filter.language')) {
        $query->where('a.language IN (' . $db->quote(JFactory::getLanguage()->getTag()) . ' ,' . $db->quote("*") . ' )');
        //  }
        // Filter by author
        $authorId = $this->getState('filter.author_id');
        if (is_numeric($authorId)) {
            $type = $this->getState('filter.author_id.include', true) ? '= ' : '<>';
            $query->where('a.created_by ' . $type . (int) $authorId);
        }

        // Filter by access level.
        $access = $this->getState('filter.access');
        if (!empty($access)) {
            $query->where('a.access = ' . (int) $access);
        }

        // Implement View Level Access
        $user = JFactory::getUser();
        if (!$user->authorise('core.admin')) {
            $groups = implode(',', $user->getAuthorisedViewLevels());
            //      $query->where('a.access IN (' . $groups . ')');
        }

        $nullDate = $db->quote($db->getNullDate());
        $nowDate = $db->quote(JFactory::getDate()->toSql());

        // Filter by start and end dates.
                 $tab = JRequest::getInt('tab');
        if ($tab == 1){
            $query->where('(a.publish_up = ' . $nullDate . ' OR a.publish_up <= ' . $nowDate . ')')
                    ->where('(a.publish_down = ' . $nullDate . ' OR a.publish_down >= ' . $nowDate . ')');
        }elseif ($tab == 2){
            $query->where('(a.publish_down != ' . $nullDate . ' AND a.publish_down <= ' . $nowDate . ')');
        }
        // $query->where('(a.publish_up = ' . $nullDate . ' OR a.publish_up <= ' . $nowDate . ')')
        //         ->where('(a.publish_down = ' . $nullDate . ' OR a.publish_down >= ' . $nowDate . ')');
        // Add list oredring and list direction to SQL query
        $sort = $this->getState('list.ordering', 'id');
        $order = 'DESC'; //$this->getState('list.direction', 'DESC');
        $query->order($db->escape($sort) . ' ' . $db->escape($order));

        return $query;
    }

    /**
     * Build a list of authors
     *
     * @return  array
     *
     * @since   1.6
     */
    public function getAuthors() {
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Construct the query
        $query->select('u.id AS value, u.name AS text')
                ->from('#__users AS u')
                ->join('INNER', '#__polls AS a ON a.created_by = u.id')
                ->group('u.id, u.name')
                ->order('u.name');

        // Setup the query
        $db->setQuery($query);

        // Return the result
        return $db->loadObjectList();
    }

    /**
     * Method to get an array of data items.
     *
     * @return  mixed  An array of data items on success, false on failure.
     *
     * @since   12.2
     */
    public function getItems() {
        if ($items = parent::getItems()) {
            //Do any procesing on fields here if needed
        }

        return $items;
    }

}

?>