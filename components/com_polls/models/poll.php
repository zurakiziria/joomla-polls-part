<?php

/**
 * @author		
 * @copyright	
 * @license		
 */
defined("_JEXEC") or die("Restricted access");

/**
 * Item Model for company.
 *
 * @package     Poll
 * @subpackage  Models
 */
class PollsModelPoll extends JModelAdmin {

    /**
     * @var        string    The prefix to use with controller messages.
     * @since   1.6
     */
    protected $text_prefix = 'COM_POLL';

    /**
     * The type alias for this content type.
     *
     * @var      string
     * @since    3.2
     */
    public $typeAlias = 'com_polls.poll';

    /**
     * Method to test whether a record can be deleted.
     *
     * @param   object    $record    A record object.
     *
     * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
     * @since   1.6
     */
    protected function canDelete($record) {
        if (!empty($record->id)) {
            if ($record->published != -2) {
                return false;
            }


            $user = JFactory::getUser();
            return $user->authorise('core.delete', $this->typeAlias . '.' . (int) $record->id);
        }
    }

    /**
     * Method to test whether a record can have its state edited.
     *
     * @param   object    $record    A record object.
     *
     * @return  boolean  True if allowed to change the state of the record. Defaults to the permission set in the component.
     * @since   1.6
     */
    protected function canEditState($record) {
        $user = JFactory::getUser();

        // Check for existing item.
        if (!empty($record->id)) {
            return $user->authorise('core.edit.state', $this->typeAlias . '.' . (int) $record->id);
        }
        // Default to component settings if neither article nor category known.
        else {
            return parent::canEditState($this->option);
        }
    }

    /**
     * Prepare and sanitise the table data prior to saving.
     *
     * @param   JTable    A JTable object.
     *
     * @return  void
     * @since   1.6
     */
    protected function prepareTable($table) {
        // Set the publish date to now
        $db = $this->getDbo();

        if ($table->published == 1 && (int) $table->publish_up == 0) {
            $table->publish_up = JFactory::getDate()->toSql();
        }

        if ($table->published == 1 && intval($table->publish_down) == 0) {
            $table->publish_down = $db->getNullDate();
        }

        // Increment the content version number.
        $table->version++;
    }

    /**
     * Auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @return  void
     *
     * @since   1.6
     */
    protected function populateState() {
        $app = JFactory::getApplication('site');

        // Load the User state.
        $pk = $app->input->getInt('id');
        $this->setState($this->getName() . '.id', $pk);

        // Load the parameters.
        $params = JComponentHelper::getParams('com_polls');
        $this->setState('params', $params);
    }

    /**
     * Alias for JTable::getInstance()
     *
     * @param   string  $type    The type (name) of the JTable class to get an instance of.
     * @param   string  $prefix  An optional prefix for the table class name.
     * @param   array   $config  An optional array of configuration values for the JTable object.
     *
     * @return  mixed    A JTable object if found or boolean false if one could not be found.
     */
    public function getTable($type = 'Poll', $prefix = 'PollsTable', $config = array()) {
        return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * Method for getting the form from the model.
     *
     * @param   array    $data      Data for the form.
     * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
     *
     * @return  mixed  A JForm object on success, false on failure
     */
    public function getForm($data = array(), $loadData = true) {
        JForm::addFormPath(JPATH_COMPONENT_ADMINISTRATOR . '/models/forms');
        JForm::addFieldPath(JPATH_COMPONENT_ADMINISTRATOR . '/models/fields');

        JForm::addRulePath(JPATH_COMPONENT_ADMINISTRATOR . '/models/rules');

        $options = array('control' => 'jform', 'load_data' => $loadData);
        $form = $this->loadForm($this->typeAlias, $this->name, $options);

        if (empty($form)) {
            return false;
        }

        return $form;
    }

    /**
     * Method to get the data that should be injected in the form.
     *
     * @return  array    The default data is an empty array.
     */
    protected function loadFormData() {
        $app = JFactory::getApplication();
        $data = $app->getUserState($this->option . '.edit.' . $this->name . '.data', array());

        if (empty($data)) {
            $data = $this->getItem();
        }

        return $data;
    }

    /**
     * Method to get a single record.
     *
     * @param	integer	The id of the primary key.
     *
     * @return	mixed	Object on success, false on failure.
     * @since	1.6
     */
    public function getItem($pk = null) {
        if (!$item = parent::getItem($pk)) {
            throw new Exception('Failed to load item');
        }

        return $item;
    }

    public function getPollAnswers($id,$ord = 0){
        if (!$id){
            return false;
        }
        $db = JFactory::getDbo();
        
        $query = 'SELECT a.*,(SELECT COUNT(*) FROM #__polls_vote WHERE answer_id = a.id AND state = 1) as answer_votes, (SELECT COUNT(*) FROM #__polls_vote WHERE poll_id = a.poll_id AND state = 1) as poll_votes FROM #__polls_answers AS a WHERE a.poll_id = '.$id.' AND a.state = 1';
        if (!$ord){
        $query .= ' ORDER BY a.ordering DESC ';
        }elseif ($ord == 1){
        $query .= ' ORDER BY a.ordering ASC ';    
        }else{
        $query .= ' ORDER BY answer_votes DESC ';    
        }
        $db->setQuery($query);
        $pl = $db->loadObjectList();
        
        if ($pl){
            foreach ($pl as $key=>$p){
           
                
                $pl[$key]->percent = $p->poll_votes == 0 || $p->answer_votes == 0 ? 0 : round((100 / $p->poll_votes) * $p->answer_votes);
            }
        }

        return $pl;
    }
    public function getPoll(){
        
        $db = JFactory::getDbo();

        $id = JRequest::getInt('id', 0);

        if(!$id){
            return array();
        }
        // Define null and now dates
		$nullDate = $db->quote($db->getNullDate());
		$nowDate  = $db->quote(JFactory::getDate()->toSql());
                
                $query = "SELECT a.*,(SELECT COUNT(*) FROM #__polls_vote WHERE poll_id = a.id AND state = 1) as votes "
                        . "FROM #__polls AS a "
                        . ""
                        . "WHERE a.state = 1 AND a.id = ".$id
                        
                        . " AND a.language IN (" . $db->quote(JFactory::getLanguage()->getTag()) . " ," . $db->quote('*') . " )";
                $db->setQuery($query);
                $res = $db->loadObject();
                if ($res){
                    $res->params = clone $this->getState('params');
                    return $res;
                }
                
    }
    /**
     * Increment the hit counter for the item.
     *
     * @param   integer  $pk  Optional primary key of the item to increment.
     *
     * @return  boolean  True if successful; false otherwise and internal error set.
     */
    public function hit($pk = 0) {
        $input = JFactory::getApplication()->input;
        $hitcount = $input->getInt('hitcount', 1);

        if ($hitcount) {
            $pk = (!empty($pk)) ? $pk : (int) $this->getState('poll.id');

            $table = $this->getTable();
            $table->load($pk);
            $table->hit($pk);
        }

        return true;
    }



    public function statPollhits() {
        $db = $this->getDbo();
        $id = JRequest::getInt('id');
        if (!$id) {
            return false;
        }
        $nullDate = $db->quote($db->getNullDate());
        $nowDate = $db->quote(JFactory::getDate()->toSql());

        $query = ' SELECT sum(t.hits) AS pollhits FROM #__polls AS t'
               . ' t.id = '.$id
                . ' AND t.state = 1'
                . ' AND t.language IN (' . $db->quote(JFactory::getLanguage()->getTag()) . ' ,' . $db->quote("*") . ' )';
        $db->setQuery($query);
        $item = $db->LoadObject();
        return $item->pollhits;
    }

    

}

?>