<?php

/**
 * @author		
 * @copyright	
 * @license		
 */
defined("_JEXEC") or die("Restricted access");

/**
 * Company item view class.
 *
 * @package     Company
 * @subpackage  Views
 */
class PollsViewAddPoll extends JViewLegacy {

    protected $item;
    protected $form;
    protected $state;

    public function display($tpl = null) {

        $app = JFactory::getApplication();
        $user = JFactory::getUser();
        $levels = $user->getAuthorisedViewLevels();
        $this->item = $this->get('Poll');
        $this->category = $this->get('Category');
        

        parent::display($tpl);
    }

}

?>