<?php
defined("_JEXEC") or die("Restricted access");
jimport('tilklib.helper.helper');
jimport('tilklib.helper.pollhelper');
jimport('tilklib.image.img');
$today = JFactory::getDate();
$userTz = JFactory::getUser()->getParam('timezone');
$timeZone = JFactory::getConfig()->get('offset');

if ($userTz) {
    $timeZone = $userTz;
}

$tz = new DateTimeZone($timeZone);
$date = JFactory::getDate($today, $tz);
$Itemid = tilkHelper::getItemid('com_polls', 'polls');
$content_params = JComponentHelper::getParams('com_polls');
$user = JFactory::getUser();
$doc = JFactory::getDocument();
$js = ' jQuery(document).ready(function () {'
        . 'jQuery("#answer-block").sortable({        '
        . ' update: function (event, ui) {'
        . ''
        . ''
        . '}'
        . '});'
        . 'jQuery("#answer-block").disableSelection();'
        . ''
        . 'jQuery("#poll_button").click(function(){'
        . 'jQuery("#poll-form").submit();'
        . '});'
        . ''
        . 'jQuery("#form_image").change(function(){
            console.log("ariis");
                      readURL(this, "imagesrc");
                });'
        . 'function readURL(input, div) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            jQuery("#"+div).attr("src", e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}'
        . 'var dateToday = new Date();'
        . 'jQuery("#form_time").datetimepicker({
            format:"Y-m-d",
            showButtonPanel: true,
            minDate: dateToday 
        });'
        . ''
        . ' jQuery("#add_newfield").click(function () {
                                    var html = "";
                            var idx = jQuery(".answer").length + 1;
                        
        if (idx < 32) {'
        . 'html += "<div class=\"poll_form_block\" id=\""+idx+"\">";
        html += "<span class=\"poll_answer\">"; 
        html += "<label for=\"form_answer_"+idx+"\">"; 
        html += "' . JText::_('Answer') . ' "+idx+"*"; 
        html += "</label>"; 
        html += "<div class=\"poll_form_block_in\">"; 
        html += "<input type=\"text\" name=\"answer[]\" class=\"answer\">"; 
        html += "<input type=\"text\" name=\"aid[]\" value=\"0\" style=\"display:none;\">"; 
        html += "</div>"; 
        html += "</span>"; 
        html += "<span class=\"poll_image\">"; 
        html += "<label for=\"form_image_"+idx+"\"  onclick=\"jQuery(\"#form_image"+idx+"\").click();\">"; 
        html += "' . JText::_('MAIN_IMAGE') . '"; 
        html += "</label>"; 
        html += "<span>"; 
        html += "<img onclick=\"jQuery(\'#form_image"+idx+"\').click();\" src=\"images/default.png\" id=\"imagesrc"+idx+"\" width=\"150px\" height=\"100px\" />"; 
        html += "</span>"; 
        html += "<div class=\"poll_form_block_in\">"; 
        html += "<input type=\"file\" name=\"image1[]\" id=\"form_image"+idx+"\" class=\"poll_form_input\"  />"; 
        html += "<input type=\"hidden\" name=\"answer_image[]\"  />"; 
        html += "</div>"; 
        html += "</span>"; 
        html += "</div>"; 
        jQuery("#answer-block").append(html);
        jQuery("#form_image"+idx).change(function(){
            console.log("ariis");
                      readURL(this, "imagesrc"+idx);
                });
       }'
        . '});'
        . ''
       
        . 'jQuery(".poll_form_input").change(function(){
           var idn = jQuery(this).parent().parent().find("img").attr("id");
                      readURL(this, idn);
                });'
        . '});';

$doc->addScriptDeclaration($js);
?>

<div class="addpoll-container">
    <?php
    if ($this->item->id) {
        $task = 'addpoll.add';
    } else {
        $task = 'addpoll.add';
    }
    if ($user->id) {
        $disabled = '';
        $form_start = '<form action="' . JRoute::_('index.php?') . '" method="POST" class="form" id="poll-form" enctype="multipart/form-data">';
        $form_end = '';
        $form_end .= '<div class="poll_inputs_btn">
                            <button type="button" class="poll button_add" id="poll_button">' . JText::_('Add_Poll') . '</button>
                            <input type="hidden" name="id" value="' . $this->item->id . '" />
                            <input type="hidden" name="option" value="com_polls" />
                            <input type="hidden" name="task" value="' . $task . '" />
                                ' . JHtml::_('form.token') . '
                        </div>';
        $form_end .= '</form>';
    } else {
        $disabled = 'disabled="disabled"';
        $form_start = '<div id="form-control" class="row">';
        $form_end = '</div>';
    }
    ?>
    <div class="container">
        <?php
        echo $form_start;
        ?>
        <div class="poll_form_image col-md-3">
            <label for="form_image_main"  <?php echo (!$disabled) ? 'onclick="jQuery(\'#form_image\').click();"' : '' ?>>
                <?php echo JText::_('MAIN_IMAGE'); ?>
            </label>
            <span><img  <?php echo (!$disabled) ? 'onclick="jQuery(\'#form_image\').click();"' : '' ?> src="<?php echo ($this->item->image1) ? $this->item->image1 : 'images/default.png'; ?>" id="imagesrc" width="150px" height="100px" /></span>
            <div class="poll_form_block_in">
                <input type="file" name="image1[0]" id="form_image" class="poll_form_input"  />
                <input type="hidden" name="main_image" value="<?php echo $this->item->image1; ?>"  />
            </div>
        </div>


        <div class="poll_form_block col-md-3">
            <label for="form_title">
                <?php echo JText::_('QUESTION'); ?>*
            </label>
            <div class="poll_form_block_in">
                <input type="text" name="title" class="form-control"  <?php echo $disabled; ?> id="form_title" class="poll_form_input" value="<?php echo $this->item->title; ?>" />
            </div>
        </div>
        <?php
        if ($this->category) {
            $cats = pollHelper::getRelCategoryArray($this->item->id);

            ?>
            <div class="poll_form_block col-md-3">
                <label for="form_title">
                    <?php echo JText::_('Category'); ?>*
                </label>
                <div class="poll_form_block_in">
                    <select name="category[]" id="example" <?php echo $disabled; ?> class="form-control"  multiple="multiple" style="display: none;">

                        <?php
                        

                        foreach ($this->category as $cat) {
                            $checked = '';
                            if ($cats && in_array($cat->id, $cats)) {
                                $checked = 'selected';
                            }
                            ?>
                            <option value="<?php echo $cat->id; ?>" <?php echo $checked; ?>><?php echo $cat->title; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <script>jQuery(document).ready(function () {
                            jQuery("#example").bsMultiSelect();
                        });</script>
                </div>
            </div>
            <?php
        }
        ?>
        <div class="poll_form_block col-md-3">
            <label for="form_state">
                <?php echo JText::_('State'); ?>*
            </label>
            <div class="poll_form_block_in">
                <select name="state" id="form_state" <?php echo $disabled; ?> class="form-control">
                    <option value="0" <?php echo $this->item->state ? '' : 'selected'; ?>>No</option>
                    <option value="1"  <?php echo $this->item->state ? 'selected' : ''; ?>>Yes</option>
                </select>
            </div>
        </div>
        <div class="poll_form_block col-md-3">
            <label for="form_show_result">
                <?php echo JText::_('Show_results'); ?>*
            </label>
            <div class="poll_form_block_in">
                <select name="show_result" id="form_show_result" <?php echo $disabled; ?> class="form-control">
                    <option value="0" <?php echo $this->item->show_result ? '' : 'selected'; ?>>No</option>
                    <option value="1"  <?php echo $this->item->show_result ? 'selected' : ''; ?>>Yes</option>
                </select>
            </div>
        </div>
        <div class="poll_form_block col-md-3">
            <label for="form_layout">
                <?php echo JText::_('Layout'); ?>*
            </label>
            <div class="poll_form_block_in">
                <select name="layout" id="form_layout" <?php echo $disabled; ?> class="form-control">
                    <option value="list" <?php echo $this->item->layout == 'list' ? 'selected' : ''; ?>>List</option>
                    <option value="blocks"  <?php echo $this->item->layout == 'blocks' ? 'selected' : ''; ?>>Blocks</option>
                </select>
            </div>
        </div>
        <div class="poll_form_block col-md-3">
            <label for="form_time">
                <?php echo JText::_('DATE_END'); ?>
            </label>
            <div class="poll_form_block_in">
                <input type="text" name="time" id="form_time" class="poll_form_input form-control" value="<?php echo str_replace(' 00:00:00', '', $this->item->publish_down); ?>" />
            </div>
        </div>
        <div class="poll_form_articles col-md-12">
            <label for="form_text">
                <?php echo JText::_('Description'); ?>
            </label>
            <div class="poll_form_block_in">

                <?php
// IMPORT EDITOR CLASS
                jimport('joomla.html.editor');

// GET EDITOR SELECTED IN GLOBAL SETTINGS
                $config = JFactory::getConfig();
                $global_editor = $config->get('editor');

// GET USER'S DEFAULT EDITOR
                $user_editor = JFactory::getUser()->getParam("editor");

                if ($user_editor && $user_editor !== 'JEditor') {
                    $selected_editor = $user_editor;
                } else {
                    $selected_editor = $global_editor;
                }

// INSTANTIATE THE EDITOR
                $editor = JEditor::getInstance($selected_editor);

// SET EDITOR PARAMS
                $params = array('smilies' => '0',
                    'style' => '1',
                    'layer' => '0',
                    'table' => '0',
                    'clear_entities' => '0',
                    'mode' => '0',
                    'image' => '0'
                );

// DISPLAY THE EDITOR (name, html, width, height, columns, rows, bottom buttons, id, asset, author, params)

                if (!$disabled) {
                    echo $editor->display('text', $this->item->text, '800', '250', '20', '20', false, false, false, false, $params);
                } else {
                    echo $this->item->text;
                }
                ?>

            </div>
        </div>
        <div id="answer-block">
            <?php
            $answers = pollHelper::getPollAnswers($this->item->id, $this->item->ord_by);
            if ($answers) {

                $a = 1;
                foreach ($answers as $row) {
                    ?>
                    <div class="poll_form_block" id="<?php echo $a; ?>">
                        <span class="poll_answer">
                            <label for="form_answer_<?php echo $a; ?>">
                                <?php echo JText::_('Answer') . ' ' . $a; ?>*
                            </label>
                            <div class="poll_form_block_in">
                                <input type="text" name="answer[]" class="answer" value="<?php echo $row->title; ?>">
                                <input type="text" name="aid[]" value="<?php echo $row->id; ?>" style="display:none;">
                            </div>
                        </span>
                        <span class="poll_image">
                            <label for="form_image_<?php echo $a; ?>"  <?php echo (!$disabled) ? 'onclick="jQuery(\'#form_image' . $a . '\').click();"' : '' ?>>
                                <?php echo JText::_('ANSWER_IMAGE'); ?>
                            </label>
                            <span><img  <?php echo (!$disabled) ? 'onclick="jQuery(\'#form_image' . $a . '\').click();"' : '' ?> src="<?php echo ($row->image1) ? $row->image1 : 'images/default.png'; ?>" id="imagesrc<?php echo $a; ?>" width="150px" height="100px" /></span>
                            <div class="poll_form_block_in">
                                <input type="file" name="image1[]" id="form_image<?php echo $a; ?>" class="poll_form_input"  />
                                <input type="hidden" name="answer_image[]" value="<?php echo $row->image1; ?>"  />
                            </div>
                        </span>
                    </div>
                    <?php
                    $a++;
                }
            }
            ?>
        </div>
        <div id="add_newfield" class="btn btn-primary"><i class="fa fa-plus"></i> <?php echo JText::_('Add_new_answer'); ?></div>
        <?php
        echo $form_end;
        ?>
    </div>
</div>
<?php 


