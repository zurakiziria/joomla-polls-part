<?php

/**
 * @author		
 * @copyright	
 * @license		
 */
defined("_JEXEC") or die("Restricted access");

/**
 * Companies list view class.
 *
 * @package     Polls
 * @subpackage  Views
 */
class PollsViewCategory extends JViewLegacy {

    protected $items;
    protected $pagination;
    protected $state;
    protected $toolbar;

    public function display($tpl = null) {
        
        $app = JFactory::getApplication();

        $this->items = $this->get('Items');
        $this->state = $this->get('State');
        $this->pagination = $this->get('Pagination');
        $this->user = JFactory::getUser();
        $this->filterForm = $this->get('FilterForm');
        $this->activeFilters = $this->get('ActiveFilters');


        $active = $app->getMenu()->getActive();
        $menus = $app->getMenu();
        $pathway = $app->getPathway();
        $title = null;
        if ($active) {
            $this->params = $active->params;
        } else {
            $this->params = new JRegistry();
        }

        // Prepare the data.
//        foreach ($this->items as $item) {
//            $item->slug = $item->alias ? ($item->id . ':' . $item->alias) : $item->id;
//
//            $temp = new JRegistry;
//            $temp->loadString($item->params);
//
//            $active = $app->getMenu()->getActive();
//            $item->params = clone($this->params);
//            $item->params->merge($temp);
//        }

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors));
            return false;
        }
        $menu = $menus->getActive();

        if ($menu) {
            $this->params->def('page_heading', $this->params->get('page_title', $menu->title));
        }

        $title = $this->params->get('page_title', '');

        $id = (int) @$menu->query['id'];

        // Check for empty title and add site name if param is set
        if (empty($title)) {
            $title = $app->get('sitename');
        } elseif ($app->get('sitename_pagetitles', 0) == 1) {
            $title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
        } elseif ($app->get('sitename_pagetitles', 0) == 2) {
            $title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
        }


        $this->document->setTitle($title);


        if ($this->params->get('menu-meta_description')) {
            $this->document->setDescription($this->params->get('menu-meta_description'));
        }

        if ($this->params->get('menu-meta_keywords')) {
            $this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
        }

        if ($this->params->get('robots')) {
            $this->document->setMetadata('robots', $this->params->get('robots'));
        }

        parent::display($tpl);
    }

}

?>