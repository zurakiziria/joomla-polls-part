<?php
defined("_JEXEC") or die("Restricted access");
jimport('tilklib.helper.helper');
jimport('tilklib.helper.pollhelper');
jimport('tilklib.image.img');
$Itemid = tilkHelper::getItemid('com_polls', 'poll');
$pItemid = tilkHelper::getItemid('com_polls', 'category');
$id = JRequest::getInt('id');
$app = JFactory::getApplication();
$active = $app->getMenu()->getActive();
$uri = JURI::base().$active->alias;
$content_params = JComponentHelper::getParams('com_polls');
$w = $content_params->get('listing_image_width', 500);
$h = $content_params->get('listing_image_height', 400);
$doc = JFactory::getDocument();

//   $doc->addCustomTag('<meta property="og:image" content="http://tourlist.ge/images/mainfbimage.jpg" />');
//   $doc->addCustomTag('<link rel="image_src" href="http://tourlist.ge/images/mainfbimage.jpg" / >');
?>

<div class="maincontainer_polls">
    <?php
    if ($this->items) {
        $polls = $this->items;
        $arrayROW = array();
        $i = 0;
        foreach ($this->items as $row) {

            $date = $row->publish_down;
            $jdate = JFactory::getDate($date);

            if (strtotime($jdate->format('Y-m-d H:i:s', true)) > time()) {
                $arrayROW[$i]['time'] = strtotime($jdate->format('Y-m-d H:i:s', true)) - time();
                $arrayROW[$i]['id'] = $row->id;
                $i++;
            }
        }

        $arrayROWJS = '';
        if ($arrayROW) {
            $arrayROWJS = json_encode($arrayROW);
        }
        $js = "
                        jQuery(document).ready(function(){
                        addDate(" . $arrayROWJS . ")
                    });

                    function addDate(dateArray){
                       if(dateArray){
                          dateArray.forEach(function(entry) {
                             
                             console.log(entry);
                             setInterval(function () {
                             timer = entry.time;
                             day = parseInt(entry.time / 60 / 60 / 24, 10);
                             hour = parseInt((entry.time - day*24*60*60)  / 60 / 60, 10);
                             minutes = parseInt((entry.time - day*24*60*60 - hour*60*60) / 60, 10);
                             seconds = parseInt(entry.time % 60, 10);

                             day = day < 10 ? '0' + day : day;
                             hour = hour < 10 ? '0' + hour : hour;
                             minutes = minutes < 10 ? '0' + minutes : minutes;
                             seconds = seconds < 10 ? '0' + seconds : seconds;
        
                             day_var = '';
                             hour_var = '';
                             minutes_var = '';
                             seconds_var = '';
        
                             day_var = day + ' " . JText::_('Day') . " ';
                             hour_var = ' '+hour+':';
                             minutes_var = minutes+':';
                             seconds_var = seconds;
                             jQuery('#date_'+entry.id).html(day_var + ' ' + hour_var + minutes_var + seconds_var);
                             if (--entry.time < 0) {
                                 entry.time = 0;
                             }
                            }, 1000);
                           });
                       }
                    }
                    ";
        $doc->addScriptDeclaration($js);
    }
        ?>
        <div class="polls">
             <div class="controll-tabs">
                <span class='ctab <?php echo empty(JRequest::getInt('tab')) ? "active":""; ?>'><a href="<?php echo $uri; ?>"><?php echo JText::_('all'); ?></a></span>
                <span class='ctab <?php echo JRequest::getInt('tab') == 1 ? "active":""; ?>'><a href="<?php echo $uri.'/?tab=1'; ?>"><?php echo JText::_('current'); ?></a></span>
                <span class='ctab <?php echo JRequest::getInt('tab') == 2 ? "active":""; ?>'><a href="<?php echo $uri.'/?tab=2'; ?>"><?php echo JText::_('ending'); ?></a></span>
            </div>
            <?php
            if ($this->items) {
            ?>
            <div class="row">
                <?php
                foreach ($polls as $poll) {
                    $link = JRoute::_('index.php?option=com_polls&view=poll&id=' . $poll->id . '&Itemid=' . $Itemid);
                    $date = $poll->publish_down;
                    $jdate = JFactory::getDate($date);
                    ?>

                    <div class='poll-block col-md-3' id='pollvote'>
                        <div class="poll-block-inner">

                            <div class='poll-block-mainimage-tolist'>
                                <?php
                                if ($poll->image1) {

                                    $resize = new Image;
                                    $img = $resize->crop($poll->image1, $w, $h);
                                    ?>
                                    <a href="<?php echo tilkHelper::getPollLink($poll->id, $poll->title); ?>"><img src='<?php echo JRoute::_('/' . $img); ?>' title='<?php echo $poll->title; ?>' alt='poll image'></a>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="poll-block-info">
                                <?php
                                if (strtotime($jdate->format('Y-m-d H:i:s', true)) > time()) {
                                    ?>

                                    <div class="countdown" id="date_<?php echo $poll->id; ?>"></div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="countdown"><?php echo JText::_('Ended'); ?></div>
                                    <?php
                                }
                                ?>
                                <div class='poll-block-title'><a href="<?php echo tilkHelper::getPollLink($poll->id, $poll->title); ?>" title="<?php echo $poll->title; ?>"><?php echo $poll->title; ?></a></div>
                                <?php
                                $ans = pollHelper::getPollLiderAnswer($poll->id);
                                if ($ans) {
                                    ?>
                                    <div class='poll-block-lideranswer'>
                                        <?php
                                        ?>
                                        <div class='poll-block-lideranswers-block d-flex justify-content-between'>
                                            <div class='poll-block-lideranswers-list'><i class="fa fa-star" aria-hidden="true"></i> <?php echo $ans->title; ?></div>
                                            <div class='poll-block-vote-list'><?php echo $ans->votes; ?> <i class="fa fa-thumbs-up" aria-hidden="true"></i></div>
                                        </div>
                                        <?php
                                        ?>
                                    </div>
                                    <?php
                                } else {
                                    echo JText::_('WILL_BE_SOON');
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                    <?php
                }
                ?>
                <div class="pagination">
                    <?php echo $this->pagination->getListFooter(); ?>
                </div>
            </div>
                    <?php
    }
    ?>
        </div>
</div>