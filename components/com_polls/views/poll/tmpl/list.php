<?php
defined("_JEXEC") or die("Restricted access");
jimport('tilklib.helper.helper');
jimport('tilklib.helper.pollhelper');
jimport('tilklib.image.img');
$today = JFactory::getDate();
$userTz = JFactory::getUser()->getParam('timezone');
$timeZone = JFactory::getConfig()->get('offset');
$poll = $this->item;
if ($userTz) {
    $timeZone = $userTz;
}

$tz = new DateTimeZone($timeZone);
$date = JFactory::getDate($today, $tz);
$Itemid = tilkHelper::getItemid('com_polls', 'polls');
$content_params = JComponentHelper::getParams('com_polls');
$w = $content_params->get('inner_image_width', 700);
$h = $content_params->get('inner_image_height', 420);
$aw = $content_params->get('answer_image_width', 95);
$ah = $content_params->get('answer_image_height', 90);
$share_w = $content_params->get('share_image_width', 500);
$share_h = $content_params->get('share_image_height', 340);
$doc = JFactory::getDocument();
$doc->setTitle(tilkHelper::word_limit($poll->title, 50, 1).' | Funny Polls | POLLIGRAM');
$fbto = JRequest::getInt('fbto', 0);

if ($fbto) {
    $panswer = pollHelper::getPollAnswer($fbto);

    if (isset($panswer->title)) {
        $doc->addCustomTag('<meta property="og:title" content="Polligram - My choice: ' . $panswer->title . '" />');
        $doc->addCustomTag('<meta property="og:url" content="' . JUri::root() . tilkHelper::getPollLink($poll->id, $poll->title) . '?fbto=' . $panswer->id . '" />');
    } else {
        $doc->addCustomTag('<meta property="og:title" content="' . tilkHelper::word_limit($poll->title, 50, 1) . '" />');
        $doc->addCustomTag('<meta property="og:url" content="' . JUri::root() . tilkHelper::getPollLink($poll->id, $poll->title) . '" />');
    }
} else {
    $doc->addCustomTag('<meta property="og:title" content="' . tilkHelper::word_limit($poll->title, 50, 1) . '" />');
    $doc->addCustomTag('<meta property="og:url" content="' . JUri::root() . tilkHelper::getPollLink($poll->id, $poll->title) . '" />');
}

if ($poll->image1) {
    $resize = new Image;
    $img = $resize->resize($poll->image1, $share_w, $share_h);
    $doc->addCustomTag('<meta property="og:image" content="' . JUri::root() . $img . '" />');
    $doc->addCustomTag(' <meta property="og:image:width" content="' . $share_w . '" />');
    $doc->addCustomTag('<meta property="og:image:height" content="' . $share_h . '" />');
    $doc->addCustomTag('<link rel="image_src" href="' . JUri::root() . $img . '" / >');
}
//    $doc->addCustomTag('<meta name="author" content="' . $poll->username . '">');
if ($poll->text) {
    $doc->addCustomTag('<meta property="og:description" content="' . tilkHelper::word_limit($poll->text, 90, 1) . '">');
    $doc->setDescription(tilkHelper::word_limit($poll->text, 200, 1));
}
//   $doc->addCustomTag('<meta property="og:image" content="http://tourlist.ge/images/mainfbimage.jpg" />');
//   $doc->addCustomTag('<link rel="image_src" href="http://tourlist.ge/images/mainfbimage.jpg" / >');
$voted = pollHelper::isVote();
?>

<div class="maincontainer_polls">
    <?php
    if ($this->item) {


        $arrayROW = array();
        $i = 0;


        $dater = $poll->publish_down;
        $jdate = JFactory::getDate($dater);

        if (strtotime($jdate->format('Y-m-d H:i:s', true)) > time()) {
            $arrayROW[$i]['time'] = strtotime($jdate->format('Y-m-d H:i:s', true)) - time();
            $arrayROW[$i]['id'] = $poll->id;
        }


        $arrayROWJS = '';
        if ($arrayROW) {
            $arrayROWJS = json_encode($arrayROW);
        }
        $js = "
                        jQuery(document).ready(function(){
                        addDate(" . $arrayROWJS . ")
                    });

                    function addDate(dateArray){
                       if(dateArray){
                          dateArray.forEach(function(entry) {
                             
                             console.log(entry);
                             setInterval(function () {
                             timer = entry.time;
                             day = parseInt(entry.time / 60 / 60 / 24, 10);
                             hour = parseInt((entry.time - day*24*60*60)  / 60 / 60, 10);
                             minutes = parseInt((entry.time - day*24*60*60 - hour*60*60) / 60, 10);
                             seconds = parseInt(entry.time % 60, 10);

                             day = day < 10 ? '0' + day : day;
                             hour = hour < 10 ? '0' + hour : hour;
                             minutes = minutes < 10 ? '0' + minutes : minutes;
                             seconds = seconds < 10 ? '0' + seconds : seconds;
        
                             day_var = '';
                             hour_var = '';
                             minutes_var = '';
                             seconds_var = '';
        
                             day_var = day + ' " . JText::_('Day') . " ';
                             hour_var = ' '+hour+':';
                             minutes_var = minutes+':';
                             seconds_var = seconds;
                             jQuery('#date_'+entry.id).html(day_var + ' ' + hour_var + minutes_var + seconds_var);
                             if (--entry.time < 0) {
                                 entry.time = 0;
                             }
                            }, 1000);
                           });
                       }
                    }
                    ";
        $doc->addScriptDeclaration($js);
        ?>
        <div class="polls list-layout row">
            <div class="col-md-12 mb-5 first-poll">
                <?php
                ?>
                <div class='popular-poll' id='pollvote-<?php echo $poll->id; ?>'>


                    <?php
                    $answers = pollHelper::getPollAnswers($poll->id, $poll->ord_by);

                    if ($answers) {
                        ?>
                        <div class="row">
                            <div class="col-md-7 d-flex flex-column justify-content-between">
                                <div>
                                    <div class="d-flex justify-content-between">
                                        <h2 class="popular-poll-title"><a><?php echo $poll->title; ?></a></h2>
                                        <div class="ml-3">
                                            <div class="popular-poll-end-date">

                                                <?php
                                                if (strtotime($jdate->format('Y-m-d H:i:s', true)) > time()) {
                                                    ?>

                                                    <div class="countdown" id="date_<?php echo $poll->id; ?>"></div>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <div class="countdown"><?php echo JText::_('Ended'); ?></div>
                                                    <?php
                                                }
                                                ?>
                                            </div>  

                                            <div class="poll-statistic nav">


                                                <div class="poll-statistic-block">
                                                    <span><i class="fa fa-thumbs-up" aria-hidden="true"></i></span>
                                                    <span>
                                                        <?php
                                                        echo $poll->votes;
                                                        ?>
                                                    </span>
                                                </div>
                                                <div class="poll-statistic-block">
                                                    <span><i class="fa fa-eye" aria-hidden="true"></i></span>
                                                    <span>
                                                        <?php
                                                        echo $poll->hits;
                                                        ?>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="popular-poll-answers">
                                        <div class="popular-poll-answers-items">
                                            <?php
                                            foreach ($answers as $ans) {
                                                ?>
                                                <label class='popular-poll-answers-item' for="answer-<?php echo $ans->id; ?>">
                                                    <?php
                                                    if ($ans->image1) {

                                                        $resize = new Image;
                                                        $img = $resize->crop($ans->image1, $aw, $ah);
                                                        ?>
                                                        <span class="answer-image">
                                                            <img src='<?php echo JRoute::_('/' . $ans->image1); ?>' width="120" title='<?php echo $ans->title; ?>' alt='<?php echo $ans->title; ?>'>
                                                        </span>
                                                        <?php
                                                    }
                                                    ?> 
                                                    <b>
                                                        <?php echo $ans->title; ?>
                                                    </b>  
                                                    <span id="votec-<?php echo $ans->id; ?>">
                                                        <?php
                                                        if ($voted || strtotime($poll->publish_down) && strtotime($poll->publish_down) <= strtotime($date)) {
                                                            $percent = $ans->percent . '%';
                                                            echo ' - ' . $ans->votes;
                                                            $votenum = $ans->percent . '%';
                                                            $votecolor = 'background-color: rgb(46, 182, 144)';
                                                        } else {
                                                            $percent = '';
                                                            $votenum = '100%';
                                                            $votecolor = 'transparent';
                                                        }
                                                        ?>
                                                    </span>
                                                    <input type='radio' name='question' value='<?php echo $ans->id; ?>' id='answer-<?php echo $ans->id; ?>' style='display: none;'>
                                                    <div class="popular-poll-answers-item-bar checkmark">
                                                        <div class="popular-poll-answers-item-bar-percent" style="width: <?php echo $votenum; ?>; background-color: <?php echo $votecolor; ?>;">
                                                            <span><?php echo $percent; ?></span>
                                                        </div>
                                                    </div>

                                                    <?php
                                                    //if (strtotime($poll->publish_down) && strtotime($poll->publish_down) >= strtotime($date)) {
                                                    ?>
                                                    <?php
                                                    // }
                                                    ?>
                                                </label>

                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="d-flex justify-content-between mt-3 mobilevote">
                                        <div class="ml-5 sharer-voted">
                                            <?php
                                            if ($voted) {
                                                ?>

                                                <?php
                                                if (!$fbto) {
                                                    ?>
                                                    <a href="http://www.facebook.com/share.php?u=<?php echo JUri::getInstance(); ?>?fbto=<?php echo $voted; ?>" target="_blank"> <button class="btn btn-social btn-facebook"><span class="icon icon-facebook"></span>Share my vote</button></a>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <a href="http://www.facebook.com/share.php?u=<?php echo JUri::getInstance(); ?>" target="_blank"> <button class="btn btn-social btn-facebook"><span class="icon icon-facebook"></span>Share my vote</button></a>
                                                    <?php
                                                }
                                                ?>

                                                <?php
                                            } else {
                                                if (strtotime($poll->publish_down) && strtotime($poll->publish_down) >= strtotime($date)) {
                                                    ?>
                                                    <div class="popular-poll-button" onclick='pollvote(<?php echo $poll->id; ?>, "<?php echo JSession::getFormToken(); ?>", this)'><?php echo JText::_('Vote'); ?></div>
                                                    <?php
                                                }
                                            }
                                            ?> 
                                        </div>

                                        <div class="nextpoll">
                                            <?php
                                            $module = JModuleHelper::getModule('mod_nextpoll');

                                            if ($module) {
                                                echo JModuleHelper::renderModule($module);
                                            }
                                            ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="poll-side col-md-5">
                                <div class="popular-poll-image mb-5">
                                    <?php
                                    if ($poll->image1) {

                                        $resize = new Image;
                                        $img = $resize->crop($poll->image1, $w, $h);
                                        ?>
                                        <img src='<?php echo JRoute::_('/' . $img); ?>' title='<?php echo $poll->title; ?>' alt='poll image'>
                                        <?php
                                    }
                                    ?> 
                                </div>

                                <div class="sharethis-inline-share-buttons"></div>
                                <?php
                                if (!empty($this->item->tags->itemTags)) {
                                    $this->item->tagLayout = new JLayoutFile('joomla.content.tags');
                                    echo $this->item->tagLayout->render($this->item->tags->itemTags);
                                }
                                ?>

                            </div>
                        </div>
                        <?php
                    } else {
                        echo "Will be soon";
                    }
                    ?>

                </div>
                <div class="clr"></div>
            </div>
        </div>

        <?php
    }
    ?>
</div>

<div></div>
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&autoLogAppEvents=1&version=v3.2';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!--<div class="fb-comments" data-href="https://polligram.ge<?php echo tilkHelper::getPollLink($poll->id, $poll->title); ?>" data-numposts="6" data-width="1000"></div>-->

<?php
$module = JModuleHelper::getModule('relatedpolls');

echo JModuleHelper::renderModule($module);
?>
