<?php
defined("_JEXEC") or die("Restricted access");
jimport('tilklib.helper.helper');
jimport('tilklib.helper.pollhelper');
jimport('tilklib.image.img');
$today = JFactory::getDate();
$userTz = JFactory::getUser()->getParam('timezone');
$timeZone = JFactory::getConfig()->get('offset');
$poll = $this->item;
if ($userTz) {
    $timeZone = $userTz;
}

$tz = new DateTimeZone($timeZone);
$date = JFactory::getDate($today, $tz);
$Itemid = tilkHelper::getItemid('com_polls', 'polls');
$content_params = JComponentHelper::getParams('com_polls');
$w = $content_params->get('inner_image_width', 700);
$h = $content_params->get('inner_image_height', 420);
$aw = $content_params->get('answer_image_width', 95);
$ah = $content_params->get('answer_image_height', 90);
$share_w = $content_params->get('share_image_width', 500);
$share_h = $content_params->get('share_image_height', 340);
$doc = JFactory::getDocument();
$doc->setTitle(tilkHelper::word_limit($poll->title, 50, 1));
$fbto = JRequest::getInt('fbto', 0);

if ($fbto) {
    $panswer = pollHelper::getPollAnswer($fbto);

    if (isset($panswer->title)) {
        $doc->addCustomTag('<meta property="og:title" content="Polligram - My choice: ' . $panswer->title . '" />');
        $doc->addCustomTag('<meta property="og:url" content="'.JUri::root() . tilkHelper::getPollLink($poll->id, $poll->title) . '?fbto='.$panswer->id.'" />');
    } else {
        $doc->addCustomTag('<meta property="og:title" content="' . tilkHelper::word_limit($poll->title, 50, 1) . '" />');
        $doc->addCustomTag('<meta property="og:url" content="'.JUri::root() . tilkHelper::getPollLink($poll->id, $poll->title) . '" />');
    }
} else {
    $doc->addCustomTag('<meta property="og:title" content="' . tilkHelper::word_limit($poll->title, 50, 1) . '" />');
    $doc->addCustomTag('<meta property="og:url" content="'.JUri::root() . tilkHelper::getPollLink($poll->id, $poll->title) . '" />');
}

if ($poll->image1) {
    $resize = new Image;
    $img = $resize->resize($poll->image1, $share_w, $share_h);
    $doc->addCustomTag('<meta property="og:image" content="'.JUri::root() . $img . '" />');
    $doc->addCustomTag(' <meta property="og:image:width" content="' . $share_w . '" />');
    $doc->addCustomTag('<meta property="og:image:height" content="' . $share_h . '" />');
    $doc->addCustomTag('<link rel="image_src" href="'.JUri::root() . $img . '" / >');
}
//    $doc->addCustomTag('<meta name="author" content="' . $poll->username . '">');
if ($poll->text) {
    $doc->addCustomTag('<meta property="og:description" content="' . tilkHelper::word_limit($poll->text, 90, 1) . '">');
    $doc->setDescription(tilkHelper::word_limit($poll->text, 200, 1));
}
//   $doc->addCustomTag('<meta property="og:image" content="http://tourlist.ge/images/mainfbimage.jpg" />');
//   $doc->addCustomTag('<link rel="image_src" href="http://tourlist.ge/images/mainfbimage.jpg" / >');
$voted = pollHelper::isVote();

?>

<div class="maincontainer_polls">
    <?php
    if ($this->item) {


        $arrayROW = array();
        $i = 0;


        $dater = $poll->publish_down;
        $jdate = JFactory::getDate($dater);

        if (strtotime($jdate->format('Y-m-d H:i:s', true)) > time()) {
            $arrayROW[$i]['time'] = strtotime($jdate->format('Y-m-d H:i:s', true)) - time();
            $arrayROW[$i]['id'] = $poll->id;
        }


        $arrayROWJS = '';
        if ($arrayROW) {
            $arrayROWJS = json_encode($arrayROW);
        }
        $js = "
                        jQuery(document).ready(function(){
                        addDate(" . $arrayROWJS . ")
                    });

                    function addDate(dateArray){
                       if(dateArray){
                          dateArray.forEach(function(entry) {
                             
                             console.log(entry);
                             setInterval(function () {
                             timer = entry.time;
                             day = parseInt(entry.time / 60 / 60 / 24, 10);
                             hour = parseInt((entry.time - day*24*60*60)  / 60 / 60, 10);
                             minutes = parseInt((entry.time - day*24*60*60 - hour*60*60) / 60, 10);
                             seconds = parseInt(entry.time % 60, 10);

                             day = day < 10 ? '0' + day : day;
                             hour = hour < 10 ? '0' + hour : hour;
                             minutes = minutes < 10 ? '0' + minutes : minutes;
                             seconds = seconds < 10 ? '0' + seconds : seconds;
        
                             day_var = '';
                             hour_var = '';
                             minutes_var = '';
                             seconds_var = '';
        
                             day_var = day + ' " . JText::_('Day') . " ';
                             hour_var = ' '+hour+':';
                             minutes_var = minutes+':';
                             seconds_var = seconds;
                             jQuery('#date_'+entry.id).html(day_var + ' ' + hour_var + minutes_var + seconds_var);
                             if (--entry.time < 0) {
                                 entry.time = 0;
                             }
                            }, 1000);
                           });
                       }
                    }
                    ";
        $doc->addScriptDeclaration($js);
        ?>
        <div class="polls">
            <?php
            ?>
            <div class='poll-block' id='pollvote-<?php echo $poll->id; ?>'>
                <div class='inner-poll-block-mainimage'>
                    <div class="inner-poll-block d-flex flex-column">
                        <div class='inner-poll-block-title'><?php echo $poll->title; ?></div>

                        <?php
                        $answers = pollHelper::getPollAnswers($poll->id,$poll->ord_by);
                        
                        if ($answers) {
                            ?>
                            <div class='poll-block-answers'>
                                <?php
                                foreach ($answers as $ans) {
                                    ?>
                                    <div class='poll-block-answers-block'>


                                        <?php
                                        //if (strtotime($poll->publish_down) && strtotime($poll->publish_down) >= strtotime($date)) {
                                            ?>
                                            <label for="answer-<?php echo $ans->id; ?>" class="poll-block-answers-list">
                                                <?php
                                                if ($ans->image1) {

                                                    $resize = new Image;
                                                    $img = $resize->resize($ans->image1, $aw, $ah);
                                                    ?>
                                                    <span class="answer-image">
                                                        <img src='<?php echo JRoute::_('/' . $ans->image1); ?>' width="120" title='<?php echo $ans->title; ?>' alt='<?php echo $ans->title; ?>'>
                                                    </span>
                                                    <?php
                                                }
                                                ?> 
                                                <span class="answer-title"> <?php echo $ans->title; ?> </span> <span class="poll-block-vote-list-count" id="votec-<?php echo $ans->id; ?>"><?php
                                                    if ($voted || strtotime($poll->publish_down) && strtotime($poll->publish_down) <= strtotime($date)) {
                                                        echo $ans->percent . '%';
                                                        $votenum = $ans->percent . '%';
                                                        $votecolor = 'rgba(202, 46, 27, 0.35)';
                                                    } else {
                                                        $votenum = '100%';
                                                        $votecolor = 'transfarent';
                                                    }
                                                    ?></span>
                                                <input type='radio' name='question' value='<?php echo $ans->id; ?>' id='answer-<?php echo $ans->id; ?>' style='display: none;'>
                                                <span style="width: <?php echo $votenum; ?>; background-color: <?php echo $votecolor; ?>;" class="checkmark"></span>
                                            </label>
                                            <?php
                                       // }
                                        ?>
                                    </div>

                                    <?php
                                }
                                ?>
                            </div>

                            <div class="poll-bottom-info d-flex justify-content-between">

                                <?php
                                if (strtotime($poll->publish_down) && strtotime($poll->publish_down) >= strtotime($date)) {
                                    ?>
                                    <div class='poll-block-button btn-default' onclick='pollvote(<?php echo $poll->id; ?>,"<?php echo JSession::getFormToken(); ?>",this)'><i class="fa fa-thumbs-up" aria-hidden="true"></i> <?php echo JText::_('Vote'); ?></div>
                                    <?php
                                }
                                ?> 

                                <?php
                                if (strtotime($jdate->format('Y-m-d H:i:s', true)) > time()) {
                                    ?>

                                    <div class="countdown" id="date_<?php echo $poll->id; ?>"></div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="countdown"><?php echo JText::_('Ended'); ?></div>
                                    <?php
                                }
                                ?>


                            </div>

                            <?php
                            if ($poll->image1) {

                                $resize = new Image;
                                $img = $resize->resize($poll->image1, $w, $h);
                                ?>
                                <div class="cover-image">
                                    <img src='<?php echo JRoute::_('/' . $img); ?>' title='<?php echo $poll->title; ?>' alt='poll image'>
                                </div>
                                <?php
                            }
                            ?> 

                            <?php
                        } else {
                            echo JText::_('WILL_BE_SOON');
                        }
                        ?>



                    </div>
                </div>
                <div class="poll-statistic nav">
                    <div class="poll-statistic-block">
                        <span><i class="fa fa-thumbs-up" aria-hidden="true"></i></span>
                        <span>
                            <?php
                            echo $poll->votes;
                            ?>
                        </span>
                    </div>
                    <div class="poll-statistic-block">
                        <span><i class="fa fa-eye" aria-hidden="true"></i></span>
                        <span>
                            <?php
                            echo $poll->hits;
                            ?>
                        </span>
                    </div>
                </div>
                <div class="sharer-voted">
                    <?php
                    if ($voted) {
                        if (!$fbto){
                        ?>
                        <a href="http://www.facebook.com/share.php?u=<?php echo JUri::getInstance(); ?>?fbto=<?php echo $voted; ?>" target="_blank"> <button class="btn btn-social btn-facebook"><span class="icon icon-facebook"></span>არჩეულის გაზიარება</button></a>
                        <?php
                        }else{
                            ?>
                        <a href="http://www.facebook.com/share.php?u=<?php echo JUri::getInstance(); ?>" target="_blank"> <button class="btn btn-social btn-facebook"><span class="icon icon-facebook"></span>არჩეულის გაზიარება</button></a>
                        <?php
                        }
                     
                    }
                    ?>
                </div>
            </div>
            <div class="poll-side">
                <div class="sharethis-inline-share-buttons"></div>
                <?php
                if (!empty($this->item->tags->itemTags)) {
                    $this->item->tagLayout = new JLayoutFile('joomla.content.tags');
                    echo $this->item->tagLayout->render($this->item->tags->itemTags);
                }
                ?>

            </div>
            <div class="clr"></div>
        </div>

        <?php
    }
    ?>
</div>

<div></div>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&autoLogAppEvents=1&version=v3.2';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-comments" data-href="https://polligram.ge<?php echo tilkHelper::getPollLink($poll->id, $poll->title); ?>" data-numposts="6" data-width="100%"></div>

<?php
$module = JModuleHelper::getModule('relatedpolls');

echo JModuleHelper::renderModule($module);


?>
