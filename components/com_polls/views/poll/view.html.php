<?php

/**
 * @author		
 * @copyright	
 * @license		
 */
defined("_JEXEC") or die("Restricted access");

/**
 * Company item view class.
 *
 * @package     Company
 * @subpackage  Views
 */
class PollsViewPoll extends JViewLegacy {

    protected $item;
    protected $form;
    protected $state;

    public function display($tpl = null) {
        $this->state = $this->get('State');
        $this->item = $this->get('Poll');
        $this->form = $this->get('Form');

        $this->stat = $this->get('Statistic');
        $app = JFactory::getApplication();
        $user = JFactory::getUser();
        $levels = $user->getAuthorisedViewLevels();
        $item = $this->item;
        $item->tagLayout = new JLayoutFile('joomla.content.tags');
        // Check if item is empty
        if (empty($this->item)) {
            $app->redirect(JRoute::_('index.php?option=com_polls&view=polls'), JText::_('JERROR_NO_ITEMS_SELECTED'));
        }
        $item->tags = new JHelperTags;
        $item->tags->getItemTags('com_polls.poll', $this->item->id);
        // Check item access
//        if ($this->item->id && !in_array($this->item->access, $levels)) {
//            throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
//        }
        // Is the user allowed to create an item?
//        if (!$this->item->id && !$user->authorise("core.create", "com_polls")) {
//            throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
//        }
        // Get menu params
        $menu = $app->getMenu();
        $active = $menu->getActive();

        if (is_object($active)) {
            $this->state->params = $active->params;
        }

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors));
            return false;
        }

      
        if (isset($active->query['layout'])) {
            $this->setLayout($active->query['layout']);
        }
        // Check for alternative layout of article
        elseif ($layout = $item->layout) {
            $this->setLayout($layout);
        }
        
        // Increment hits
        $model = $this->getModel();
        $model->hit($this->item->id);

        parent::display($tpl);
    }

}

?>