<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Tours
 * @author     Gaga Gelashvili <gaga_nba@yahoo.com>
 * @copyright  2017 Gaga Gelashvili
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JLoader::registerPrefix('Polls', JPATH_SITE . '/components/com_polls/');

/**
 * Class ToursRouter
 *
 * @since  3.3
 */
class PollsRouter extends JComponentRouterBase {

    /**
     * Build method for URLs
     * This method is meant to transform the query parameters into a more human
     * readable form. It is only executed when SEF mode is switched on.
     *
     * @param   array  &$query  An array of URL arguments
     *
     * @return  array  The URL arguments to use to assemble the subsequent URL.
     *
     * @since   3.3
     */
    public function build(&$query) {
        $segments = array();
        $view = null;

        if (isset($query['task'])) {
            $taskParts = explode('.', $query['task']);
            $segments[] = implode('/', $taskParts);
            $view = $taskParts[0];
            unset($query['task']);
        }

        if (isset($query['view'])) {
            $segments[] = $query['view'];
            $view = $query['view'];

            unset($query['view']);
        }


        if (isset($query['id'])) {
            if ($view == 'polls') {
                $ka_array = array('ა', 'ბ', 'გ', 'დ', 'ე', 'ვ', 'ზ', 'თ', 'ი', 'კ', 'ლ', 'მ', 'ნ', 'ო', 'პ', 'ჟ', 'რ', 'ს', 'ტ', 'უ', 'ფ', 'ქ', 'ღ', 'ყ', 'შ', 'ც', 'ჩ', 'ძ', 'წ', 'ჭ', 'ხ', 'ჯ', 'ჰ');

                $en_array = array('a', 'b', 'g', 'd', 'e', 'v', 'z', 't', 'i', 'k', 'l', 'm', 'n', 'o', 'p', 'j', 'r', 's', 't', 'u', 'f', 'q', 'g', 'y', 'sh', 'ts', 'ch', 'dz', 'w', 'ch', 'kh', 'j', 'h');

                $ru_array = array('а', 'г', 'г', 'д', 'е', 'в', 'з', 'т', 'и', 'к', 'л', 'м', 'н', 'о', 'п', 'ж', 'р', 'с', 'т', 'у', 'ф', '!@##@!', '!@##@!', '!@##@!', 'ш', 'ц', 'ч', '!@##@!', '!@##@!', '!@##@!', 'х', '!@##@!', '!@##@!');
                $dd_ru = array('ё', 'й', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
                $dd_en = array('e', 'i', 'sh', '', 'i', '', 'e', 'iu', 'ia');
                
                if (strpos($query['id'], ':') === false) {
                    //$query['id'] = (int) JRequest::getInt('id');
                    $db = JFactory::getDbo();
                    $dbQuery = $db->getQuery(true)
                            ->select('title')
                            ->from('#__company')
                            ->where('id=' . (int) $query['id']);
                    $db->setQuery($dbQuery);
                    $alias = $db->loadResult();

                    $url = preg_replace('~[^\\pL0-9_]+~u', '-', $alias);
                    $url = trim($url, "-");
                    $url = strtolower($url);


                    $url = str_replace($dd_ru, $dd_en, $url);
                    $url = str_replace($ka_array, $en_array, $url);
                    $url = str_replace($ru_array, $en_array, $url);

                    $query['id'] = $query['id'] . ':' . $url;
                } else {
                    $query['id'] = str_replace($dd_ru, $dd_en, $query['id']);
                    $query['id'] = str_replace($ka_array, $en_array, $query['id']);
                    $query['id'] = str_replace($ru_array, $en_array, $query['id']);
                }
            }
            if ($view !== null) {
                $segments[] = $query['id'];
            } else {
                $segments[] = $query['id'];
            }

            $total = count($segments);

            for ($i = 0; $i < $total; $i++) {
                $segments[$i] = str_replace(':', '-', $segments[$i]);
            }

            unset($query['id']);
        }


        return $segments;
    }

    /**
     * Parse method for URLs
     * This method is meant to transform the human readable URL back into
     * query parameters. It is only executed when SEF mode is switched on.
     *
     * @param   array  &$segments  The segments of the URL to parse.
     *
     * @return  array  The URL attributes to be used by the application.
     *
     * @since   3.3
     */
    public function parse(&$segments) {
        $vars = array();


        // View is always the first element of the array
        $vars['view'] = array_shift($segments);

        while (!empty($segments)) {
            $segment = array_pop($segments);


            // If it's the ID, let's put on the request
            if ($vars['view'] == 'poll') {
                $vars['id'] = $segment;
            } else {
                if (is_numeric($segment)) {
                    $vars['id'] = $segment;
                } else {
                    $vars['task'] = $vars['view'] . '.' . $segment;
                }
            }
        }

        return $vars;
    }

    function pollsBuildRoute(&$query) {
        $router = new CompanyRouter;

        return $router->build($query);
    }

    function pollsParseRoute($segments) {
        $router = new CompanyRouter;

        return $router->parse($segments);
    }

}
