<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Polls', JPATH_COMPONENT);
JLoader::register('PollsController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Polls');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
