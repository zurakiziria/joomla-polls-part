<?php
/**
 * @author		
 * @copyright	
 * @license		
 */

defined("_JEXEC") or die("Restricted access");

/**
 * Companies list controller class.
 *
 * @package     Polls
 * @subpackage  Controllers
 */
class PollsControllerPolls extends JControllerAdmin
{
	/**
	 * The URL view list variable.
	 *
	 * @var    string
	 * @since  12.2
	 */
	protected $view_list = 'polls';
	
	/**
	 * Get the admin model and set it to default
	 *
	 * @param   string           $name    Name of the model.
	 * @param   string           $prefix  Prefix of the model.
	 * @param   array			 $config  The model configuration.
	 */
	public function getModel($name = 'Polls', $prefix='PollsModel', $config = array())
	{
		$config['ignore_request'] = true;
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
        
            public function searchbox() {
        jimport('tilklib.helper.helper');
        $Itemid = tilkHelper::getItemid('com_polls', 'poll');
        $user = JFactory::getUser();
        $db = JFactory::getDBO();
        $value = JRequest::getVar('val', '');
        $limit = JRequest::getVar('limit', '5');
        $sort = JRequest::getVar('sort', 'a.id');
        $order = JRequest::getVar('order', 'DESC');
        $type = JRequest::getVar('type', 0);
        
        $value = htmlspecialchars(strip_tags($value));
        $html = '';
        if ($value && (mb_strlen($value) > 2 && mb_strlen($value) < 130)) {
            $nullDate = $db->quote($db->getNullDate());
            $nowDate = $db->quote(JFactory::getDate()->toSql());
        if ($type == 'current') {
           $addsql = ' AND a.publish_down > ' . $nowDate . ' OR a.publish_down = ' . $nullDate;
        } elseif ($type == 'finished') {
             $addsql = ' AND a.publish_down <= ' . $nowDate . '';
        }else{
            $addsql = '';
        }
            // Filter by start and end dates.

            $query = 'SELECT a.*,(SELECT COUNT(*) FROM #__polls_vote WHERE poll_id = a.id AND state = 1) as votes FROM `#__polls` AS a WHERE `a`.`title` LIKE "%' . $value . '%" AND `a`.`state` = 1 '.$addsql.' ORDER BY '.$sort.' '.$order.' LIMIT '.$limit;
            $db->setQuery($query);
            $list = $db->loadObjectList();

            if ($list) {
                foreach ($list as $val) {
                    $html .= '<div class="listvalue">';
                    $html .= '<a href="' . tilkHelper::getPollLink($val->id, $val->title) . '">' . $val->title . '</a>';
                    $html .= '</div>';
                }
                $msg = array('msg' => $html);
            } else {

                $msg = array('msg' => '<div class="listvalue">' . JText::_('EMPTY') . '</div>');
            }
        } else {
            $msg = array('msg' => '<div class="listvalue">' . JText::_('EMPTY') . '</div>');
        }
        echo json_encode($msg);
        die;
    }
}
?>