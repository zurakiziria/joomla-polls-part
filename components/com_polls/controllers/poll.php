<?php

/**
 * @author		
 * @copyright	
 * @license		
 */
defined("_JEXEC") or die("Restricted access");

/**
 * Poll item controller class.
 *
 * @package     Polls
 * @subpackage  Controllers
 */
class PollsControllerPoll extends JControllerForm {

    /**
     * The URL view item variable.
     *
     * @var    string
     * @since  12.2
     */
    protected $view_item = 'poll';

    /**
     * The URL view list variable.
     *
     * @var    string
     * @since  12.2
     */
    protected $view_list = 'polls';

    /**
     * Method override to check if you can edit an existing record.
     *
     * @param   array   $data  An array of input data.
     * @param   string  $key   The name of the key for the primary key; default is id.
     *
     * @return  boolean
     *
     * @since   1.6
     */
    protected function allowEdit($data = array(), $key = 'id') {
        $recordId = (int) isset($data[$key]) ? $data[$key] : 0;
        $user = JFactory::getUser();
        $userId = $user->get('id');
        $asset = 'com_polls.poll.' . $recordId;

        // Check general edit permission first.
        if ($user->authorise('core.edit', $asset)) {
            return true;
        }

        // Fallback on edit.own.
        // First test if the permission is available.
        if ($user->authorise('core.edit.own', $asset)) {
            // Now test the owner is the user.
            $ownerId = (int) isset($data['created_by']) ? $data['created_by'] : 0;
            if (empty($ownerId) && $recordId) {
                // Need to do a lookup from the model.
                $record = $this->getModel()->getItem($recordId);

                if (empty($record)) {
                    return false;
                }

                $ownerId = $record->created_by;
            }

            // If the owner matches 'me' then do the test.
            if ($ownerId == $userId) {
                return true;
            }
        }
        // If users can`t edit own items, there is no more way to get edit permissions
        else {
            return false;
        }

        // Since there is no asset tracking, revert to the component permissions.
        return parent::allowEdit($data, $key);
    }

    /**
     * Increment the hit counter for the newsfeed.
     *
     * @param   int  $pk  Optional primary key of the item to increment.
     *
     * @return  boolean  True if successful; false otherwise and internal error set.
     *
     * @since   3.0
     */
    public function hit($pk = 0) {
        $input = JFactory::getApplication()->input;
        $hitcount = $input->getInt('hitcount', 1);

        if ($hitcount) {
            $pk = (!empty($pk)) ? $pk : (int) $this->getState('newsfeed.id');
            $db = $this->getDbo();

            $db->setQuery(
                    'UPDATE #__newsfeeds' .
                    ' SET hits = hits + 1' .
                    ' WHERE id = ' . (int) $pk
            );

            try {
                $db->execute();
            } catch (RuntimeException $e) {
                $this->setError($e->getMessage());
                return false;
            }
        }

        return true;
    }

    public function addtovote() {
        // Check for request forgeries.
        JSession::checkToken( 'get' ) or die( 'Invalid Token' );
        $db = JFactory::getDBO();
        $id = JRequest::getInt('id');
        $checked = JRequest::getInt('val');
        $user = JFactory::getUser();
        jimport('tilklib.helper.helper');
        jimport('tilklib.helper.pollhelper');
        $id = (int) $id;
        $ip = $db->quote($_SERVER['REMOTE_ADDR']);
        $br = $db->quote($_SERVER['HTTP_USER_AGENT']);
        $timezone = new DateTimeZone('Asia/Tbilisi');
        $date = JFactory::getDate()->format('Y-m-d h:i:s', $timezone);


        $content_params = JComponentHelper::getParams('com_polls');
        $way = $content_params->get('vote_way', 1);

        if ($id && $checked) {
            if (tilkHelper::shapeSpace_block_proxy_visits()) {
                $k = array('msg' => '' . JText::_('ERROR_PROXY') . '', 'status' => 0);
                echo json_encode($k);
                die;
            }
            
            if (pollHelper::checkSpam($ip, $id)){
                 $k = array('msg' => 'Your ip is detected as spam, try later!', 'status' => 0);
                echo json_encode($k);
                die;
            }

            $pollquery = "SELECT * FROM #__polls WHERE id=" . $id . " AND state =1 ";
            $db->setQuery($pollquery);
            $pol = $db->loadObject();
            if (isset($pol->vote_way) && $pol->vote_way) {
                if ($pol->vote_way == 1) {
                    //ip
                    $way = 1;
                } elseif ($pol->vote_way == 2) {
                    //cookie
                    $way = 2;
                } elseif ($pol->vote_way == 3) {
                    //ip browser
                    $way = 0;
                }
            }
            if (isset($pol->publish_down) && strtotime($pol->publish_down) && strtotime($pol->publish_down) <= strtotime($date)) {
                $k = array('msg' => '' . JText::_('This_poll_is_over') . '', 'status' => 0);
                echo json_encode($k);
                die;
            }

            $see = 'SELECT pa.id FROM #__polls_answers AS pa '
                    . 'WHERE pa.id = ' . $checked . ' AND pa.poll_id = ' . $id;
            $db->setQuery($see);
            $com = $db->loadObject();
            if (!$com->id) {
                $k = array('msg' => '' . JText::_('This_poll_does_not_exist') . '', 'status' => 0);
            } else {
                if ($way == 1) {
                    $addsql = "`ip` = " . $ip . "";
                } elseif ($way == 3) {
                    $addsql = "`ip` = " . $ip . " AND  `agent` = " . $br . "";
                } else {
                    $addsql = "`ip` = " . $ip . " AND  `agent` = " . $br . "";
                }
                $select = "SELECT `id` FROM `#__polls_vote` WHERE `poll_id` = " . (int) $id . " AND " . $addsql . " AND state = 1";
                $db->setQuery($select);
                $res = $db->loadObject();

                $cnt = 'SELECT COUNT(*) FROM #__polls_vote WHERE poll_id = '.(int) $id.' AND ip= "'.$ip.'" AND state = 1';
                $db->setQuery($cnt);
                $all = $db->loadResult();
                if (!isset($res->id) || empty($res->id)) {
                    if ($all < 7){
                    $query = 'INSERT INTO #__polls_vote (answer_id,poll_id,ip,agent,state,date)VALUES(' . (int) $checked . ',' . (int) $id . ',' . $ip . ',' . $br . ',1,"' . $date . '")';
                    $db->setQuery($query);
                    if ($db->query()) {
                        $model = $this->getModel('poll');
                        $data = $model->getPollAnswers($id,$pol->ord_by);
                        $k = array('msg' => '' . JText::_('votedd_successfully') . '', 'status' => 1, 'data' => $data, 'id' => $checked);
                    }else{
                       $k = array('msg' => '' . JText::_('Can_not_vote') . '', 'status' => 0); 
                    }
                    } else {

                        $k = array('msg' => '' . JText::_('Can_not_vote') . '', 'status' => 0);
                    }
                } else {

                    $k = array('msg' => '' . JText::_('You_have_almost_voted') . '', 'status' => 0);
                }
            }
        } else {

            $k = array('msg' => '' . JText::_('You_must_choose_value') . '', 'status' => 0);
        }
        echo json_encode($k);
        die;
    }

}

?>