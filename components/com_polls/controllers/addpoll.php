<?php

/**
 * @author		
 * @copyright	
 * @license		
 */
defined("_JEXEC") or die("Restricted access");

/**
 * Poll item controller class.
 *
 * @package     Polls
 * @subpackage  Controllers
 */
class PollsControllerAddPoll extends JControllerForm {

    /**
     * The URL view item variable.
     *
     * @var    string
     * @since  12.2
     */
    protected $view_item = 'poll';

    /**
     * The URL view list variable.
     *
     * @var    string
     * @since  12.2
     */
    protected $view_list = 'polls';

    /**
     * Method override to check if you can edit an existing record.
     *
     * @param   array   $data  An array of input data.
     * @param   string  $key   The name of the key for the primary key; default is id.
     *
     * @return  boolean
     *
     * @since   1.6
     */
    protected function allowEdit($data = array(), $key = 'id') {
        $recordId = (int) isset($data[$key]) ? $data[$key] : 0;
        $user = JFactory::getUser();
        $userId = $user->get('id');
        $asset = 'com_polls.poll.' . $recordId;

        // Check general edit permission first.
        if ($user->authorise('core.edit', $asset)) {
            return true;
        }

        // Fallback on edit.own.
        // First test if the permission is available.
        if ($user->authorise('core.edit.own', $asset)) {
            // Now test the owner is the user.
            $ownerId = (int) isset($data['created_by']) ? $data['created_by'] : 0;
            if (empty($ownerId) && $recordId) {
                // Need to do a lookup from the model.
                $record = $this->getModel()->getItem($recordId);

                if (empty($record)) {
                    return false;
                }

                $ownerId = $record->created_by;
            }

            // If the owner matches 'me' then do the test.
            if ($ownerId == $userId) {
                return true;
            }
        }
        // If users can`t edit own items, there is no more way to get edit permissions
        else {
            return false;
        }

        // Since there is no asset tracking, revert to the component permissions.
        return parent::allowEdit($data, $key);
    }

    public function add() {

        // Check for request forgeries.
        JSession::checkToken() or die('Invalid Token');
        $id = JRequest::getInt('id');
        $input = JFactory::getApplication()->input;
        $model = $this->getModel('addpoll');
        $user = JFactory::getUser();
        jimport('tilklib.helper.helper');
        $Itemid = tilkHelper::getItemid('com_polls', 'addpoll');

        $main_image = $input->get('main_image','','STRING');
        $answer_image = $input->get('answer_image',array(),'ARRAY');
        if (!isset($user->id)) {
            $this->setRedirect(JRoute::_('index.php?option=com_polls&view=addpoll&Itemid=' . $Itemid), JText::_('YOU_HAVE_NOT_PERMISSION_TO_ADD_POLL'));
            return false;
        }

        if (empty($input->get('title', '', 'STRING')) || strlen($input->get('title', '', 'STRING')) < 2) {
            $this->setRedirect(JRoute::_('index.php?option=com_polls&view=addpoll&id=' . $id . '&Itemid=' . $Itemid), JText::_('INVALID_QUESTION'));
            return false;
        }
        if (JRequest::getVar('text', '', 'post', 'string', JREQUEST_ALLOWHTML) && strlen(JRequest::getVar('text', '', 'post', 'string', JREQUEST_ALLOWHTML)) > 17000) {
            $this->setRedirect(JRoute::_('index.php?option=com_polls&view=addpoll&id=' . $id . '&Itemid=' . $Itemid), JText::_('DESCRIPTION_IS_BIG'));
            return false;
        }
        if (!is_array($input->get('category')) || count($input->get('category')) < 1) {
            $this->setRedirect(JRoute::_('index.php?option=com_polls&view=addpoll&id=' . $id . '&Itemid=' . $Itemid), JText::_('INVALID_CATEGORY'));
            return false;
        }
        if ((!is_array($input->get('answer')) && !is_array($input->get('aid'))) || count($input->get('answer')) != count($input->get('aid')) || (count($input->get('answer')) < 1 && count($input->get('aid')) < 1)) {
            $this->setRedirect(JRoute::_('index.php?option=com_polls&view=addpoll&id=' . $id . '&Itemid=' . $Itemid), JText::_('INVALID_ANSWERS'));
            return false;
        }
        if (empty($input->get('time'))) {
            $this->setRedirect(JRoute::_('index.php?option=com_polls&view=addpoll&id=' . $id . '&Itemid=' . $Itemid), JText::_('INVALID_TIME'));
            return false;
        }
        //image
        $image1 = JRequest::getVar('image1', null, 'files', 'array');
        

        if (!is_array($image1)) {
            $this->setRedirect(JRoute::_('index.php?option=com_polls&view=addpoll&id=' . $id . '&Itemid=' . $Itemid), JText::_('INVALID_IMAGES'));
            return false;
        }

        $layout = $input->get('layout', 'list');
        $shresult = $input->get('show_result', 1);
        $state = $input->get('state', 1);
        $use_type = $input->get('use_type', 1);
        $ord_by = $input->get('ord_by', 1);
        $text = JRequest::getVar('text', '', 'post', 'string', JREQUEST_ALLOWHTML);
        $data = array();
        $data['id'] = $id;
        $data['title'] = htmlspecialchars(($input->get('title', '', 'STRING')));
        $data['publish_down'] = (!$input->get('time')) ? '0000-00-00 00:00:00' : htmlspecialchars(strip_tags($input->get('time')));
        $data['category'] = $input->get('category');
        $data['created_by'] = $user->id;
        $data['answer'] = $input->get('answer','','ARRAY');
        $data['aid'] = $input->get('aid');
        $data['layout'] = !in_array($layout, array('list', 'blocks', 'default')) ? 'list' : $layout;
        $data['show_result'] = (int) $shresult > 0 ? 1 : 0;
        $data['state'] = (int) $state > 0 ? 1 : 0;
        $data['use_type'] = (int) $use_type > 0 ? 1 : 0;
        $data['ord_by'] = (int) $ord_by > 0 ? 1 : 0;
        $data['text'] = $text;
        //array suratebis


        $imgs = array();
        if ($image1 && isset($image1['name'])) {
            for ($i = 0; $i <= count($image1['name']); $i++) {


                if (isset($image1['error'][$i]) && $image1['error'][$i] == 0) {
                    $imgs[$i] = $this->uploadFile($image1, $i, $data['created_by']);
                }
            }
        }

        if (is_array($imgs) && isset($imgs[0][1])) {
            (string) $data['image1'] = $imgs[0][1];
        } elseif (isset($imgs[0])) {
            $this->setRedirect(JRoute::_('index.php?option=com_polls&view=addpoll&id=' . $id . '&Itemid=' . $Itemid), $imgs[0]);
            return false;
        } else {
            $data['image1'] = $main_image;
            
        }

        if ($data) {
            if ($pid = $model->addPoll($data)) {

                $db = JFactory::getDBO();
                //category
                $query = 'DELETE FROM #__category_rel WHERE poll_id = ' . $pid . '';
                $db->setQuery($query);
                $db->execute();
                foreach ($data['category'] as $ct) {
                    $query = 'INSERT INTO #__category_rel (poll_id,cat_id)VALUES(' . $pid . ',' . $ct . ')';
                    $db->setQuery($query);
                    $db->execute();
                }


                $adata = array();
                $i = 0;
                $fim = 1;
                if ($data['answer']) {

                    $pollid = $pid;

                    foreach ($data['answer'] as $item) {

                        $adata[] = array(
                            'id' => isset($data['aid'][$i]) && $data['aid'][$i] ? $data['aid'][$i] : 0,
                            'title' => $item,
                            'poll_id' => $pollid,
                            'ordering' => isset($data['aid'][$i]) ? $i : 0,
                            'image1' => isset($imgs[$fim][1]) && $imgs[$fim][1] ? $imgs[$fim][1] : $answer_image[$i]
                        );
                        $i++;
                        $fim++;
                    }
                }

                if ($adata) {
                    foreach ($adata as $row) {


                        $model->addAnswers($row);
                    }
                }

            return  $this->setRedirect(JRoute::_('index.php?option=com_polls&view=addpoll&id=' . $pid . '&Itemid=' . $Itemid), JText::_('Saved'));
            }
        }
return  $this->setRedirect(JRoute::_('index.php?option=com_polls&view=addpoll&id=' . $id . '&Itemid=' . $Itemid), JText::_('Failed'));
    }


    
    
    private function uploadFile($fieldName, $index, $owner) {


        jimport('joomla.filesystem.file');
        jimport('joomla.filesystem.folder');


        $filename = JFile::makeSafe($fieldName['name'][$index]);
        $fileError = $fieldName['error'][$index];
        if ($fileError > 0) {
            switch ($fileError) {
                case 1:
                    return JText::_('FILE_TO_LARGE_THAN_PHP_INI_ALLOWS');

                case 2:
                    return JText::_('FILE_TO_LARGE_THAN_HTML_FORM_ALLOWS');

                case 3:
                    return JText::_('ERROR_PARTIAL_UPLOAD');

                case 4:
                    return '';
            }
        }

//check for filesize
        $fileSize = $fieldName['size'][$index];
        if ($fileSize > 2000000) {
            return JText::_('FILE_BIGGER_THAN_2MB');
        }

//check the file extension is ok
        $fileName = $fieldName['name'][$index];
        $uploadedFileNameParts = explode('.', $fileName);
        $uploadedFileExtension = array_pop($uploadedFileNameParts);

        $validFileExts = explode(',', 'jpeg,jpg,png,gif');

//assume the extension is false until we know its ok
        $extOk = false;

//go through every ok extension, if the ok extension matches the file extension (case insensitive)
//then the file extension is ok
        foreach ($validFileExts as $key => $value) {
            if (preg_match("/$value/i", $uploadedFileExtension)) {
                $extOk = true;
            }
        }

        if ($extOk == false) {
            return JText::_('INVALID_EXTENSION');
        }

//the name of the file in PHP's temp directory that we are going to move to our folder
        $fileTemp = $fieldName['tmp_name'][$index];

//for security purposes, we will also do a getimagesize on the temp file (before we have moved it 
//to the folder) to check the MIME type of the file, and whether it has a width and height
        $imageinfo = getimagesize($fileTemp);
        $ext = image_type_to_extension($imageinfo[2]);
//we are going to define what file extensions/MIMEs are ok, and only let these ones in (whitelisting), rather than try to scan for bad
//types, where we might miss one (whitelisting is always better than blacklisting) 
        $okMIMETypes = 'image/jpeg,image/pjpeg,image/png,image/x-png,image/gif';
        $validFileTypes = explode(",", $okMIMETypes);

//if the temp file does not have a width or a height, or it has a non ok MIME, return
        if (!is_int($imageinfo[0]) || !is_int($imageinfo[1]) || !in_array($imageinfo['mime'], $validFileTypes)) {
            return JText::_('INVALID_FILETYPE');
        }

//lose any special characters in the filename
        $fileName = time() . preg_replace("/[^A-Za-z0-9]/i", "-", $fileName);

//always use constants when making file paths, to avoid the possibilty of remote file inclusion
        $uploadPath = JPATH_SITE . '/images/mypoll/' . $owner . '/' . md5($fileName) . $ext;
        $uploadPathOk = 'images/mypoll/' . $owner . '/' . md5($fileName) . $ext;



        // Import filesystem libraries. Perhaps not necessary, but does not hurt.
        // First verify that the file has the right extension. We need jpg only..docx
        if (strtolower(JFile::getExt($filename)) == 'jpg' || strtolower(JFile::getExt($filename)) == 'png' || strtolower(JFile::getExt($filename)) == 'jpeg') {
            if (!JFile::upload($fileTemp, $uploadPath)) {
                return JText::_('ERROR_MOVING_FILE');
            } else {
                // success, exit with code 0 for Mac users, otherwise they receive an IO Error
                return array(1, $uploadPathOk);
            }
        }
    }

}

?>