<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Polls
 * @author     Zura Kiziria
 * @copyright  2017 Zura Kiziria
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Polls helper.
 *
 * @since  1.6
 */
class PollsHelper {

    /**
     * Configure the Linkbar.
     *
     * @param   string  $vName  string
     *
     * @return void
     */
    public static function addSubmenu($vName = '') {
        JHtmlSidebar::addEntry(
                JText::_('Polls'), 'index.php?option=com_polls&view=polls', $vName == 'Polls'
        );
        JHtmlSidebar::addEntry(
                JText::_('Category'), 'index.php?option=com_categories&extension=com_polls', $vName == 'Category'
        );
                JHtmlSidebar::addEntry(
                JText::_('Category Rel'), 'index.php?option=com_polls&view=catrels', $vName == 'Category Rel'
        );
        JHtmlSidebar::addEntry(
                JText::_('Answers'), 'index.php?option=com_polls&view=answers', $vName == 'Answers'
        );
        JHtmlSidebar::addEntry(
                JText::_('Votes'), 'index.php?option=com_polls&view=votes', $vName == 'Votes'
        );
        JHtmlSidebar::addEntry(
                JText::_('Groups'), 'index.php?option=com_polls&view=groups', $vName == 'Groups'
        );
        JHtmlSidebar::addEntry(
                JText::_('Types'), 'index.php?option=com_polls&view=usetypes', $vName == 'Types'
        );
    }

    /**
     * Gets the files attached to an item
     *
     * @param   int     $pk     The item's id
     *
     * @param   string  $table  The table's name
     *
     * @param   string  $field  The field's name
     *
     * @return  array  The files
     */
    public static function getFiles($pk, $table, $field) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query
                ->select($field)
                ->from($table)
                ->where('id = ' . (int) $pk);

        $db->setQuery($query);

        return explode(',', $db->loadResult());
    }

    /**
     * Gets a list of the actions that can be performed.
     *
     * @return    JObject
     *
     * @since    1.6
     */
    public static function getActions() {
        $user = JFactory::getUser();
        $result = new JObject;

        $assetName = 'com_polls';

        $actions = array(
            'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
        );

        foreach ($actions as $action) {
            $result->set($action, $user->authorise($action, $assetName));
        }

        return $result;
    }

}
