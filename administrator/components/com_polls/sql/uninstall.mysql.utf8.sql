DROP TABLE IF EXISTS `#__polls`;
DROP TABLE IF EXISTS `#__polls_answers`;

DELETE FROM `#__content_types` WHERE (type_alias LIKE 'com_polls.%');