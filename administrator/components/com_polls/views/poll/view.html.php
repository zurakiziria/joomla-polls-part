<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Polls
 * @author     Zura Kiziria
 * @copyright  2017 Zura Kiziria
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 *
 * @since  1.6
 */
class PollsViewPoll extends JViewLegacy {

    protected $state;
    protected $item;
    protected $form;

    /**
     * Display the view
     *
     * @param   string  $tpl  Template name
     *
     * @return void
     *
     * @throws Exception
     */
    public function display($tpl = null) {
        $this->state = $this->get('State');
        $this->item = $this->get('Item');
        $this->form = $this->get('Form');
        $this->answers = $this->get('Answers');

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors));
        }

        $this->addToolbar();
        parent::display($tpl);
    }

    /**
     * Add the page title and toolbar.
     *
     * @return void
     *
     * @throws Exception
     */
    protected function addToolbar() {
        JFactory::getApplication()->input->set('hidemainmenu', true);

        $user = JFactory::getUser();
        $isNew = ($this->item->id == 0);

        if (isset($this->item->checked_out)) {
            $checkedOut = !($this->item->checked_out == 0 || $this->item->checked_out == $user->get('id'));
        } else {
            $checkedOut = false;
        }

        $canDo = PollsHelper::getActions();

        JToolBarHelper::title(JText::_('COM_POLLS_TITLE_POLL'), 'poll.png');

        // If not checked out, can save the item.
        JToolBarHelper::apply('poll.apply', 'JTOOLBAR_APPLY');
        JToolBarHelper::save('poll.save', 'JTOOLBAR_SAVE');

        if (!$checkedOut && ($canDo->get('core.create'))) {
            JToolBarHelper::custom('poll.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
        }

        // If an existing item, can save to a copy.
        if (!$isNew && $canDo->get('core.create')) {
            JToolBarHelper::custom('poll.save2copy', 'save-copy.png', 'save-copy_f2.png', 'JTOOLBAR_SAVE_AS_COPY', false);
        }

        // Button for version control
        if ($this->state->params->get('save_history', 1) && $user->authorise('core.edit')) {
            JToolbarHelper::versions('com_polls.poll', $this->item->id);
        }

        if (empty($this->item->id)) {
            JToolBarHelper::cancel('poll.cancel', 'JTOOLBAR_CANCEL');
        } else {
            JToolBarHelper::cancel('poll.cancel', 'JTOOLBAR_CLOSE');
        }
    }

}
