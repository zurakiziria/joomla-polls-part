<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Polls
 * @author     Zura Kiziria
 * @copyright  2017 Zura Kiziria
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_polls/css/form.css');
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function () {

    });
    Joomla.submitbutton = function (task) {
        
        if (task == 'poll.cancel') {
            Joomla.submitform(task, document.getElementById('poll-form'));
        } else {

        if (task != 'poll.import'){
            if (task != 'poll.cancel' && document.formvalidator.isValid(document.id('poll-form'))) {

                Joomla.submitform(task, document.getElementById('poll-form'));
            } else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
            }else{
            Joomla.submitform(task, document.getElementById('answer-form'));
            }
        }
    }
</script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<form
    action="<?php echo JRoute::_('index.php?option=com_polls&layout=edit&id=' . (int) $this->item->id); ?>"
    method="post" enctype="multipart/form-data" name="adminForm" id="poll-form" class="form-validate">

    <div class="form-horizontal">
        <?php echo $this->form->renderField('title'); ?>
        <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_POLLS_TITLE_POLL', true)); ?>
        <div class="row-fluid">
            <div class="span10 form-horizontal">
                <div class="span9">
                    <fieldset class="adminform">

                        <?php echo $this->form->getInput('text'); ?>
                    </fieldset>
                </div>
                <div class="span3">
                    <?php echo $this->form->renderField('state'); ?>
                    <?php echo $this->form->renderField('usetype_id'); ?>
                    <?php echo $this->form->renderField('catid'); ?>
                    <?php echo $this->form->renderField('group_id'); ?>
                    <?php echo $this->form->renderField('show_result'); ?>
                    <?php echo $this->form->renderField('layout'); ?>
                    <?php echo $this->form->renderField('vote_way'); ?>
                    <?php echo $this->form->renderField('ord_by'); ?>
                    <?php echo $this->form->renderField('access'); ?>
                    <?php echo $this->form->renderField('ordering'); ?>
                    <?php echo $this->form->renderField('language'); ?>
                    <?php echo $this->form->renderField('tags'); ?>

                </div>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>
        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'Images', JText::_('Images', true)); ?>
        <div class="row-fluid">
            <div class="span10 form-horizontal">
                <fieldset class="adminform">
                    <div class="span6">
                        <?php echo $this->form->renderField('image1'); ?>
                    </div>
                </fieldset>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>
        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'Options', JText::_('Options', true)); ?>
        <div class="row-fluid">
            <div class="span10 form-horizontal">
                <fieldset class="adminform">
                    <div class="span6">
                        <?php echo $this->form->renderField('created'); ?>
                        <?php echo $this->form->renderField('created_by'); ?>
                        <?php echo $this->form->renderField('modified'); ?>
                        <?php echo $this->form->renderField('modified_by'); ?>
                        <?php echo $this->form->renderField('checked_out'); ?>
                        <?php echo $this->form->renderField('checked_out_time'); ?>
                        <?php echo $this->form->renderField('publish_up'); ?>
                        <?php echo $this->form->renderField('publish_down'); ?>
                    </div>
                    <div class="span6">
                        <?php echo $this->form->renderField('metakey'); ?>
                        <?php echo $this->form->renderField('metadesc'); ?>
                        <?php echo $this->form->renderField('hits'); ?> 
                        <?php echo $this->form->renderField('version'); ?>
                    </div>

                </fieldset>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>
        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'Answers', JText::_('Answers', true)); ?>
                    <div class="row-fluid">
            <div class="span10 form-horizontal">
                <fieldset class="adminform-answer">
                    <?php
                    if ($this->answers) {

                        $answers = $this->answers;
                        $i = 1;
                        foreach ($answers as $answ) {
                            ?>

                            <div class="span12" id="<?php echo $i; ?>">
                                <div class="span6">
                                    <div class="control-group">
                                        <div class="control-label">
                                            <label id="jform_modified_by-lbl" for="jform_modified_by" class="">
                                                Answer/sort (<?php echo $i; ?>)
                                            </label>
                                        </div>
                                        <div class="controls">
                                            <input type="text" title="" name="answers[]" id="jform_answers<?php echo $i; ?>" value="<?php echo $answ->title; ?>" size="22" class="hasTooltip answer" data-original-title="" aria-invalid="false">
                                        </div>
                                    </div>
                                </div>
                                <div class="span3">
                                    <div class="input-prepend input-append">
                                        <div class="media-preview add-on"><span class="hasTipPreview" title=""><i class="icon-eye"></i></span></div>
                                        <input type="text" name="image1[]" id="jform_image1<?php echo $i; ?>" value="<?php echo $answ->image1; ?>" readonly="readonly" title="" class="input-small field-media-input hasTipImgpath" data-basepath="https://polligram.net/">
                                        <a class="modal btn" title="Select" href="index.php?option=com_media&amp;view=images&amp;tmpl=component&amp;asset=com_polls&amp;author=created_by&amp;fieldid=jform_image1<?php echo $i; ?>&amp;folder=" rel="{handler: 'iframe', size: {x: 800, y: 500}}">Select</a>
                                        <a class="btn hasTooltip" title="Clear" href="#" onclick="jInsertFieldValue('', 'jform_image1<?php echo $i; ?>'); return false;">
                                            <i class="icon-remove"></i></a>


                                    </div>
                                    
                                </div>
                                <div class="span3">
                                    <div class="control-group">
                                        <div class="controls">
                                            <input type="text" title="" name="sort_order[]" id="jform_sort_order<?php echo $i; ?>" value="<?php echo $answ->ordering; ?>" size="15" maxlength="45" class="readonly hasTooltip" readonly="readonly" data-original-title="" aria-invalid="false">
                                            <span class="href"><a href="<?php echo JRoute::_('index.php?option=com_polls&view=answer&layout=edit&id='.$answ->id); ?>">ბმული</a></span>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" value="<?php echo $answ->id; ?>" name="idx[]">
                            </div>
                            <?php
                            $i++;
                            }
                            }
                            ?>
                        </fieldset>
<!--                        <div class="span6"><button id="add_poll" onclick="Joomla.submitbutton('poll.import');" class="btn btn-success">Add</button></div>-->
                        <div class="span2"><div id="add_newfield" class="btn btn-default">+</div></div>
                    </div>
                </div>
                <?php echo JHtml::_('bootstrap.endTab'); ?>

                <?php echo JHtml::_('bootstrap.endTabSet'); ?>

                <input type="hidden" name="task" value=""/>
                <?php echo JHtml::_('form.token'); ?>

                    </div>
                </form>

<!--<form action="<?php echo JRoute::_('index.php?option=com_polls&task=poll.import&id=' . (int) $this->item->id); ?>" method="post" id="answer-form">

</form>-->
                <script type="text/javascript">
                    js = jQuery.noConflict();

                    js(document).ready(function () {
                        var max = 0;
                        var min = 0;
                        var k = 1;
                        js('input[name=\'sort_order[]\']').each(function () {

                    var value = parseInt(js(this).val());
                    max = (value > max) ? value : max;
                        min = (k == 1) ? value : min;
                         k++;
                        });

                        js(function () {
                            js(".adminform-answer").sortable(); 
                        js(".adminform-answer").sortable({         update: function (event, ui) {             var getid = js(ui.item).attr('id');
                       var value = [];
                            js('input[name=\'sort_order[]\']').each(function () {

                                 value.push(parseInt(js(this).val()));
                                   
                                    });
max = Math.max.apply(Math,value); // 3
min = Math.min.apply(Math,value); // 1
                                        //        for(i = 1;js(".answer").length > i;i++){
                //          
                //        }
console.log(min)
                                        js('input[name=\'sort_order[]\']').each(function () {

                                    js(this).val(min++);
                                    });
                                }

                            });
                        });
                                    js(".adminform-answer").disableSelection();
                                        js("#add_newfield").click(function () {
                                    var html = '';
                            var idx = js(".answer").length + 1;
                        var value = max++;
                        if (idx < 32) {
                        
                                                   
                            
                            html += '<div class="span12" id="' + idx + '">';                                     
                                html += '<div class="span6">'; 
                                html += '<div class="control-group">';
                html += '<div class="control-label">';
                                html += '<label id="jform_modified_by-lbl" for="jform_modified_by" class="">';
                                html += ' Answer/sort (' + idx + ')';     
                                html += '</label>';
                                html += '</div>';
                        html += '<div class="controls">';
                    html += '<input type="text" title="" name="answers[]" id="jform_answers' + idx + '" value="" size="22" class="hasTooltip answer" data-original-title="" aria-invalid="false">';
html += '</div>';
html += '</div>';
html += '</div>';
                                html += '<div class="span3">';
                                html += '<div class="input-prepend input-append">';     
                                html += '<div class="media-preview add-on"><span class="hasTipPreview" title=""><i class="icon-eye"></i></span></div>';
                                html += '<input type="text" name="image1[]" id="jform_image1' + idx + '" value="" readonly="readonly" title="" class="input-small field-media-input hasTipImgpath" data-basepath="https://polligram.ge/">';
                        html += '<a class="modal btn" title="Select" href="index.php?option=com_media&amp;view=images&amp;tmpl=component&amp;asset=com_polls&amp;author=created_by&amp;fieldid=jform_image1' + idx + '&amp;folder=" rel="{handler: \'iframe\', size: {x: 800, y: 500}}">Select</a>';
                        html += '<a class="btn hasTooltip" title="Clear" href="#" onclick="jInsertFieldValue(\'\', \'jform_image1' + idx + '\'); return false;">';
                                html += '<i class="icon-remove"></i></a>'; 

html += '</div>';
                                html += '</div>';    
html += '<div class="span3">';
html += '<div class="control-group">';
html += '<div class="controls">';
html += '<input type="text" title="" name="sort_order[]" id="jform_sort_order' + idx + '" value="' + (value + 1) + '" size="15" maxlength="45" class="readonly hasTooltip" readonly="readonly" data-original-title="" aria-invalid="false">';
html += '</div>';
html += '</div>';
html += '</div>';
html += '</div>';
js(".adminform-answer").append(html);
}

});
});
</script>
