<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Polls
 * @author     Zura Kiziria
 * @copyright  2017 Zura Kiziria
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_polls/css/form.css');
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function () {

    });

    Joomla.submitbutton = function (task) {
        if (task == 'group.cancel') {
            Joomla.submitform(task, document.getElementById('group-form'));
        } else {

            if (task != 'group.cancel' && document.formvalidator.isValid(document.id('group-form'))) {

                Joomla.submitform(task, document.getElementById('group-form'));
            } else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>

<form
    action="<?php echo JRoute::_('index.php?option=com_polls&layout=edit&id=' . (int) $this->item->id); ?>"
    method="post" enctype="multipart/form-data" name="adminForm" id="group-form" class="form-validate">

    <div class="form-horizontal">
        <?php echo $this->form->renderField('title'); ?>
        <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('Group', true)); ?>
        <div class="row-fluid">
            <div class="span10 form-horizontal">
                <div class="span9">
                    <fieldset class="adminform">

                        <?php echo $this->form->getInput('description'); ?>
                    </fieldset>
                </div>
                <div class="span3">
                    <?php echo $this->form->renderField('usetype_id'); ?>
                    <?php echo $this->form->renderField('state'); ?>
                    <?php echo $this->form->renderField('ordering'); ?>

                    <?php echo $this->form->renderField('language'); ?>

                </div>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>
        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'Images', JText::_('Images', true)); ?>
        <div class="row-fluid">
            <div class="span10 form-horizontal">
                <fieldset class="adminform">
                    <div class="span6">
                        <?php echo $this->form->renderField('image'); ?>
                    </div>
                </fieldset>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>
        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'Options', JText::_('Options', true)); ?>
        <div class="row-fluid">
            <div class="span10 form-horizontal">
                <fieldset class="adminform">
                    <div class="span6">
                        <?php echo $this->form->renderField('created'); ?>
                        <?php echo $this->form->renderField('created_by'); ?>
                        <?php echo $this->form->renderField('checked_out'); ?>
                        <?php echo $this->form->renderField('checked_out_time'); ?>

                    </div>
                    <div class="span6">
                        <?php echo $this->form->renderField('metakey'); ?>
                        <?php echo $this->form->renderField('metadesc'); ?>
                        <?php echo $this->form->renderField('hits'); ?> 
                        <?php echo $this->form->renderField('version'); ?>
                    </div>

                </fieldset>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>

        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'senior', JText::_('Grouped', true)); ?>
        <div class="row-fluid">
            <div class="span10 form-horizontal">
                <fieldset class="adminform">
                    <div class="span6">
                        <?php
                        if ($this->polls) {
                            foreach ($this->polls as $poll) {
                                $url = JRoute::_('index.php?option=com_polls&view=poll&layout=edit&id=' . $poll->id);
                                ?>
                                <div><a href="<?php echo $url; ?>"><?php echo $poll->title; ?></a></div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </fieldset>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>

       

        <?php echo JHtml::_('bootstrap.endTabSet'); ?>

        <input type="hidden" name="task" value=""/>
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>
