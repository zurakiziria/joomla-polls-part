<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Polls
 * @author     Zura Kiziria
 * @copyright  2017 Zura Kiziria
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

use Joomla\Utilities\ArrayHelper;

/**
 * Polls list controller class.
 *
 * @since  1.6
 */
class PollsControllerPolls extends JControllerAdmin {

    /**
     * Method to clone existing Polls
     *
     * @return void
     */
    public function duplicate() {
        // Check for request forgeries
        Jsession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // Get id(s)
        $pks = $this->input->post->get('cid', array(), 'array');

        try {
            if (empty($pks)) {
                throw new Exception(JText::_('COM_POLLS_NO_ELEMENT_SELECTED'));
            }

            ArrayHelper::toInteger($pks);
            $model = $this->getModel();
            $model->duplicate($pks);
            $this->setMessage(Jtext::_('COM_POLLS_ITEMS_SUCCESS_DUPLICATED'));
        } catch (Exception $e) {
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'warning');
        }

        $this->setRedirect('index.php?option=com_polls&view=polls');
    }

    /**
     * Proxy for getModel.
     *
     * @param   string  $name    Optional. Model name
     * @param   string  $prefix  Optional. Class prefix
     * @param   array   $config  Optional. Configuration array for model
     *
     * @return  object	The Model
     *
     * @since    1.6
     */
    public function getModel($name = 'poll', $prefix = 'PollsModel', $config = array()) {
        $model = parent::getModel($name, $prefix, array('ignore_request' => true));

        return $model;
    }

    /**
     * Method to save the submitted ordering values for records via AJAX.
     *
     * @return  void
     *
     * @since   3.0
     */
    public function saveOrderAjax() {
        // Get the input
        $input = JFactory::getApplication()->input;
        $pks = $input->post->get('cid', array(), 'array');
        $order = $input->post->get('order', array(), 'array');

        // Sanitize the input
        ArrayHelper::toInteger($pks);
        ArrayHelper::toInteger($order);

        // Get the model
        $model = $this->getModel();

        // Save the ordering
        $return = $model->saveorder($pks, $order);

        if ($return) {
            echo "1";
        }

        // Close the application
        JFactory::getApplication()->close();
    }

    /**
     * Removes an item.
     *
     * @return  void
     *
     * @since   12.2
     */
    public function delete() {
        // Check for request forgeries
        JSession::checkToken() or die(JText::_('JINVALID_TOKEN'));

        // Get items to remove from the request.
        $cid = JFactory::getApplication()->input->get('cid', array(), 'array');

        if (!is_array($cid) || count($cid) < 1) {
            JLog::add(JText::_($this->text_prefix . '_NO_ITEM_SELECTED'), JLog::WARNING, 'jerror');
        } else {
            // Get the model.
            $model = $this->getModel();

            // Make sure the item ids are integers
            jimport('joomla.utilities.arrayhelper');
            JArrayHelper::toInteger($cid);
            jimport('joomla.filesystem.file');



            // Remove the items.
            if ($model->delete($cid)) {

                $poll = $model->getPoll($cid);

                $add_images = array();
                if ($poll->image1) {
                    $add_images[] = $poll->image1;
                }
                if ($poll->image2) {
                    $add_images[] = $poll->image2;
                }

                if ($poll->image3) {
                    $add_images[] = $poll->image3;
                }

                if ($poll->image4) {
                    $add_images[] = $poll->image4;
                }

                if ($poll->image5) {
                    $add_images[] = $item->image5;
                }
                for ($i = 0; $i <= 4; $i++) {

                    if (isset($add_images[$i])) {
                        $path = JPath::clean(JPATH_ROOT . '/');
                        if (JFile::exists($path . $add_images[$i])) {
                            JFile::delete($path . $add_images[$i]);
                        }
                    }
                }

                $this->setMessage(JText::plural($this->text_prefix . '_N_ITEMS_DELETED', count($cid)));
            } else {
                $this->setMessage($model->getError(), 'error');
            }

            // Invoke the postDelete method to allow for the child class to access the model.
            $this->postDeleteHook($model, $cid);
        }

        $this->setRedirect(JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_list, false));
    }

}
