<?php


// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

use Joomla\Utilities\ArrayHelper;

/**
 * Polls list controller class.
 *
 * @since  1.6
 */
class PollsControllerVotes extends JControllerAdmin
{
	/**
	 * Method to clone existing Polls
	 *
	 * @return void
	 */
	
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    Optional. Model name
	 * @param   string  $prefix  Optional. Class prefix
	 * @param   array   $config  Optional. Configuration array for model
	 *
	 * @return  object	The Model
	 *
	 * @since    1.6
	 */
	public function getModel($name = 'votes', $prefix = 'PollsModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}

	/**
	 * Method to save the submitted ordering values for records via AJAX.
	 *
	 * @return  void
	 *
	 * @since   3.0
	 */
	
        
        public function publish($pks = null, $state = 1, $userId = 0){
            $db = JFactory::getDBO();
            $post = JRequest::get('post');
            if ($post['cid']){
                if ($post['task'] == 'votes.unpublish'){
                    $state = 0;
                }
                foreach ($post['cid'] as $ds){
                    $db->setQuery(
			'UPDATE `#__polls_vote`' .
			' SET `state` = ' . (int) $state .
			' WHERE id = ' .$ds
			
		);
		$db->execute();
                }
            }
            if ($state){
            $this->setMessage(JText::_('Item Succesfuly published', count($post['cid'])));
            }else{
              $this->setMessage(JText::_('Item Succesfuly unpublished', count($post['cid'])));   
            }
         $this->setRedirect(JRoute::_('index.php?option=com_polls&view=votes', false));
        }
}
