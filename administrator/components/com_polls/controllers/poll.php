<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Polls
 * @author     Zura Kiziria
 * @copyright  2017 Zura Kiziria
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Tour controller class.
 *
 * @since  1.6
 */
class PollsControllerPoll extends JControllerForm {

    /**
     * Constructor
     *
     * @throws Exception
     */
    public function __construct() {
        $this->view_list = 'polls';
        parent::__construct();
    }

    public function import() {
        $input = JFactory::getApplication()->input;
        $answers = $input->getVar('answers');
        $images = JRequest::getVar('image1');
        $sort_order = $input->get('sort_order');
        $idx = $input->get('idx');
        $model = $this->getModel();
        $data = array();
        $i = 0;

        foreach ($answers as $item) {
 
            $data[] = array(
                'id' => isset($idx[$i]) && $idx[$i] ? $idx[$i] : 0,
                'title' => $item,
                'poll_id' => JRequest::getInt('id', 0),
                'ordering' => isset($sort_order[$i]) && $sort_order[$i] ? $sort_order[$i] : 0,
                'image1' => isset($images[$i]) && $images[$i] ? $images[$i] : ''
            );
            $i++;
        }

        if ($data) {
            foreach ($data as $row) {

                
                $model->addAnswers($row);

                
            }
        }
        
        $this->setRedirect(JRoute::_('index.php?option=com_polls&view=poll&layout=edit&id='.JRequest::getInt('id', 0)), 'Add Answers', 'success');
    }

}
