<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Polls
 * @author     Zura Kiziria
 * @copyright  2017 Zura Kiziria
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Tour controller class.
 *
 * @since  1.6
 */
class PollsControllerCatrel extends JControllerForm {

    /**
     * Constructor
     *
     * @throws Exception
     */
    public function __construct() {
        $this->view_list = 'catrels';
        parent::__construct();
    }


    private function sendJsonResponse($ret) {
        header('Content-Type: application/json');
        echo json_encode($ret);
        die;
    }

}
