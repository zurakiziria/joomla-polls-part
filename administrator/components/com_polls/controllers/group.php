<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Polls
 * @author     Zura Kiziria
 * @copyright  2017 Zura Kiziria
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Tour controller class.
 *
 * @since  1.6
 */
class PollsControllerGroup extends JControllerForm {

    /**
     * Constructor
     *
     * @throws Exception
     */
    public function __construct() {
        $this->view_list = 'groups';
        parent::__construct();
    }

    public function connect() {
        $type = JRequest::getVar('type');
        $data = new stdClass();
        $ret = new stdClass();
        $ret->status = 0;
        $ret->message = "";
        if ($type == 'app') {
            $post = JRequest::get('data', 'ARRAY');

            $data->group_id = $post['data']['group_id'];
            $data->text = $post['data']['text'];
            $data->description = $post['data']['description'];

            $model = $this->getModel();
            $conns = $model->getConnects($data->group_id);
            $is = false;

            if ($conns) {
                //check if exsist same connection
                $same = 0;
                foreach ($conns as $key => $con) {
                    $connds = $model->getConnected($con->id);
                    if ($same < count($post['data']['answers'])) {
                        $same = 0;
                    }

                    foreach ($connds as $cn) {

                        for ($i = 0; $i < count($post['data']['answers']); $i++) {

                            if ((int) $post['data']['answers'][$i] == (int) $cn->answer_id && (int) $post['data']['polls'][$i] == (int) $cn->poll_id) {
                                //if exist => true;
                                $same++;
                            }
                        }
                    }
                }
                if ($same >= count($post['data']['answers'])) {
                    $is = true;
                } else {
                    $is = false;
                }
            }
            if (!$is) {
                $con_id = $model->addConnect($data);

                if ($con_id) {
                    for ($i = 0; $i < count($post['data']['answers']); $i++) {
                        $model->addResults((int) $con_id, (int) $data->group_id, (int) $post['data']['polls'][$i], (int) $post['data']['answers'][$i]);
                    }
                    $ret->status = 1;
                    $ret->message = "კავშირი წარმატებით შესრულდა, დაელოდეთ გვერდის განახლებას";
                } else {
                    $ret->status = 0;
                    $ret->message = "კავშირის შენახვისას დაფიქსირდა შეცდომა";
                }
            } else {
                $ret->status = 0;
                $ret->message = "ასეთი კავშირი უკვე არსებობს ბაზაში";
            }
        }

        $this->sendJsonResponse($ret);
    }

    public function connectedMethod() {

        $type = JRequest::getVar('type');
        $m = JRequest::getVar('m');
        $cid = JRequest::getVar('cid');
        $text = JRequest::getVar('text');
        $stext = JRequest::getVar('stext');
        $data = new stdClass();
        $ret = new stdClass();
        $ret->status = 0;
        $ret->message = "კავშირის განახლება ვერ  მოხერხდა";
        if ($type == 'app' && $cid && $text && $stext) {
            if ($m == 'update') {
                $model = $this->getModel();
                $model->updateConnect($cid, $text, $stext);
                $ret->status = 1;
                $ret->message = "კავშირი დარედაქტირდა, დაელოდეთ გვერდის განახლებას";
            }
        }
        $this->sendJsonResponse($ret);
    }

    private function sendJsonResponse($ret) {
        header('Content-Type: application/json');
        echo json_encode($ret);
        die;
    }

}
