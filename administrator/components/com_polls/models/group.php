<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Polls
 * @author     Zura Kiziria
 * @copyright  2017 Zura Kiziria
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Polls model.
 *
 * @since  1.6
 */
class PollsModelGroup extends JModelAdmin {

    /**
     * @var      string    The prefix to use with controller messages.
     * @since    1.6
     */
    protected $text_prefix = 'COM_POLLS';

    /**
     * @var   	string  	Alias to manage history control
     * @since   3.2
     */
    public $typeAlias = 'com_polls.group';

    /**
     * @var null  Item data
     * @since  1.6
     */
    protected $item = null;

    /**
     * Returns a reference to the a Table object, always creating it.
     *
     * @param   string  $type    The table type to instantiate
     * @param   string  $prefix  A prefix for the table class name. Optional.
     * @param   array   $config  Configuration array for model. Optional.
     *
     * @return    JTable    A database object
     *
     * @since    1.6
     */
    public function getTable($type = 'Group', $prefix = 'PollsTable', $config = array()) {
        return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * Method to get the record form.
     *
     * @param   array    $data      An optional array of data for the form to interogate.
     * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
     *
     * @return  JForm  A JForm object on success, false on failure
     *
     * @since    1.6
     */
    public function getForm($data = array(), $loadData = true) {
        // Initialise variables.
        $app = JFactory::getApplication();

        // Get the form.
        $form = $this->loadForm(
                'com_polls.group', 'group', array('control' => 'jform',
            'load_data' => $loadData
                )
        );

        if (empty($form)) {
            return false;
        }

        return $form;
    }

    /**
     * Method to get the data that should be injected in the form.
     *
     * @return   mixed  The data for the form.
     *
     * @since    1.6
     */
    protected function loadFormData() {
        // Check the session for previously entered form data.
        $data = JFactory::getApplication()->getUserState('com_polls.edit.group.data', array());

        if (empty($data)) {
            if ($this->item === null) {
                $this->item = $this->getItem();
            }

            $data = $this->item;
        }

        return $data;
    }

    /**
     * Method to get a single record.
     *
     * @param   integer  $pk  The id of the primary key.
     *
     * @return  mixed    Object on success, false on failure.
     *
     * @since    1.6
     */
    public function getItem($pk = null) {
        if ($item = parent::getItem($pk)) {
            // Do any procesing on fields here if needed
        }
        return $item;
    }

    public function getAnswer($pk = null) {
        $pk = (!empty($pk)) ? $pk : (int) $this->getState($this->getName() . '.id');
        $table = $this->getTable();

        if (isset($pk[0]) && !empty($pk[0])) {
            $pk = $pk[0];
        }

        if ($pk > 0) {
            // Attempt to load the row.
            $return = $table->load($pk);

            // Check for a table object error.
            if ($return === false && $table->getError()) {
                $this->setError($table->getError());

                return false;
            }
        }

        // Convert to the JObject before adding other data.
        $properties = $table->getProperties(1);
        $item = JArrayHelper::toObject($properties, 'JObject');

        if (property_exists($item, 'params')) {
            $registry = new Registry;
            $registry->loadString($item->params);
            $item->params = $registry->toArray();
        }

        return $item;
    }

    /**
     * Method to duplicate an Poll
     *
     * @param   array  &$pks  An array of primary key IDs.
     *
     * @return  boolean  True if successful.
     *
     * @throws  Exception
     */
    public function duplicate(&$pks) {
        $user = JFactory::getUser();

        // Access checks.
        if (!$user->authorise('core.create', 'com_polls')) {
            throw new Exception(JText::_('JERROR_CORE_CREATE_NOT_PERMITTED'));
        }

        $dispatcher = JEventDispatcher::getInstance();
        $context = $this->option . '.' . $this->name;

        // Include the plugins for the save events.
        JPluginHelper::importPlugin($this->events_map['save']);

        $table = $this->getTable();

        foreach ($pks as $pk) {
            if ($table->load($pk, true)) {
                // Reset the id to create a new record.
                $table->id = 0;

                if (!$table->check()) {
                    throw new Exception($table->getError());
                }


                // Trigger the before save event.
                $result = $dispatcher->trigger($this->event_before_save, array($context, &$table, true));

                if (in_array(false, $result, true) || !$table->store()) {
                    throw new Exception($table->getError());
                }

                // Trigger the after save event.
                $dispatcher->trigger($this->event_after_save, array($context, &$table, true));
            } else {
                throw new Exception($table->getError());
            }
        }

        // Clean cache
        $this->cleanCache();

        return true;
    }

    /**
     * Prepare and sanitise the table prior to saving.
     *
     * @param   JTable  $table  Table Object
     *
     * @return void
     *
     * @since    1.6
     */
    protected function prepareTable($table) {
        jimport('joomla.filter.output');

        if (empty($table->id)) {
            // Set ordering to the last item if not set
            if (@$table->ordering === '') {
                $db = JFactory::getDbo();
                $db->setQuery('SELECT MAX(ordering) FROM #__polls_group');
                $max = $db->loadResult();
                $table->ordering = $max + 1;
            }
        }
    }

    public function getPolls() {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $id = JRequest::getInt('id');
        if (!$id){
            return false;
        }
        $query = 'SELECT a.id,a.title FROM #__polls AS a WHERE a.state = 1 AND a.group_id = ' . $id;
        $db->setQuery($query);
        $result = $db->loadObjectList();
        return $result;
    }

    public function getResultPolls() {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $id = JRequest::getInt('id');
        $query = 'SELECT a.id,a.title FROM #__polls AS a WHERE a.state = 1 AND a.group_id = ' . $id . ' ';
        $db->setQuery($query);
        $result = $db->loadObjectList();
        return $result;
    }

    public function getConnects($id = 0) {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        if (!$id) {
            $id = JRequest::getInt('id');
        }
        $query = 'SELECT a.* FROM #__polls_group_connection AS a '
                . 'WHERE a.group_id = ' . $id . ' ';
        $db->setQuery($query);
        $result = $db->loadObjectList();
        return $result;
    }

    public function getConnected($id = 0) {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        if (!$id) {
            $id = JRequest::getInt('id');
        }
        $query = 'SELECT a.*,gc.text,pl.title as poll_title, pa.title as answer_title FROM #__polls_group_result AS a '
                . 'LEFT JOIN #__polls_group_connection AS gc ON gc.id = a.connection_id AND gc.group_id = a.group_id '
                . 'LEFT JOIN #__polls AS pl ON pl.id = a.poll_id '
                . 'LEFT JOIN #__polls_answers AS pa ON pa.id = a.answer_id '
                . 'WHERE a.connection_id = ' . $id . ' ';
        $db->setQuery($query);
        $result = $db->loadObjectList();
        return $result;
    }

    public function addConnect($data = array()) {
        $db = $this->getDbo();
        $query = $db->getQuery(true);


        $query = 'INSERT INTO #__polls_group_connection (group_id,text,description)VALUES("' . $db->escape($data->group_id) . '","' . $db->escape($data->text) . '","' . $db->escape($data->description) . '")';
        $db->setQuery($query);

        $db->query();
        $connectID = $db->insertid();
        if ($connectID) {
            return $connectID;
        }
        return false;
    }

    public function addResults($con_id, $group_id, $poll_id, $ans_id) {

        $db = $this->getDbo();
        $query = $db->getQuery(true);
        if ($con_id && $group_id && $poll_id && $ans_id) {
            $query = 'INSERT INTO #__polls_group_result (group_id,poll_id,answer_id,connection_id)VALUES("' . $db->escape($group_id) . '","' . $db->escape($poll_id) . '","' . $db->escape($ans_id) . '","' . $db->escape($con_id) . '")';
            $db->setQuery($query);

            $db->execute();
            return true;
        }
        return false;
    }

    public function updateConnect($id, $text = "", $stext = "") {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        if ($id && $text && $stext) {
            $query = 'UPDATE #__polls_group_connection SET text = "' . $db->escape($text) . '", description = "' . $db->escape($stext) . '" WHERE id = ' . (int) $id;
            $db->setQuery($query);

            $db->execute();
            return true;
        }
    }

}
