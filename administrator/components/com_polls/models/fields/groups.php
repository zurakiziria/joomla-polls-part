<?php
/**
 * @package     Joomla.Libraries
 * @subpackage  Form
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

JFormHelper::loadFieldClass('list');

/**
 * Form Field class for the Joomla Framework.
 *
 * @since  3.1
 */
class JFormFieldGroups extends JFormFieldList
{
	/**
	 * A flexible tag list that respects access controls
	 *
	 * @var    string
	 * @since  3.1
	 */
	public $type = 'Groups';


	/**
	 * Method to get a list of tags
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   3.1
	 */
	protected function getOptions()
	{
		$published = $this->element['state']? $this->element['state'] : array(0,1);

		$db		= JFactory::getDbo();

                
                $query = 'SELECT id as value, title as text FROM #__polls_group WHERE state = 1 ORDER BY id DESC';
			

		// Filter language
//		if (!empty($this->element['language']))
//		{
//			$query->where('a.language = ' . $db->q($this->element['language']));
//		}




		// Get the options.
		$db->setQuery($query);


			$options = $db->loadObjectList();
if (!$options){
    return false;
}

$return = array();
		// Block the possibility to set a tag as it own parent
		if ($this->form->getName() == 'com_polls.group')
		{
			$id   = (int) $this->form->getValue('id', 0);

			foreach ($options as $option)
			{
				if ($option->value == $id)
				{
					$option->disable = true;
				}
			}
		}
                    $return[0] = new stdClass;
                    $return[0]->value = 0;
                    $return[0]->text = "აირჩიე ჯგუფი";
                           $i = 1; 
                foreach($options as $row){

                    $return[$i] = new stdClass;
                    $return[$i]->value = $row->value;
                    $return[$i]->text = $row->text;
                    $i++;
                }

		return $return;
	}

	
}
