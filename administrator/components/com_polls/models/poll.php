<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Polls
 * @author     Zura Kiziria
 * @copyright  2017 Zura Kiziria
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Polls model.
 *
 * @since  1.6
 */
class PollsModelPoll extends JModelAdmin {

    /**
     * @var      string    The prefix to use with controller messages.
     * @since    1.6
     */
    protected $text_prefix = 'COM_POLLS';

    /**
     * @var   	string  	Alias to manage history control
     * @since   3.2
     */
    public $typeAlias = 'com_polls.poll';

    /**
     * @var null  Item data
     * @since  1.6
     */
    protected $item = null;

    /**
     * Returns a reference to the a Table object, always creating it.
     *
     * @param   string  $type    The table type to instantiate
     * @param   string  $prefix  A prefix for the table class name. Optional.
     * @param   array   $config  Configuration array for model. Optional.
     *
     * @return    JTable    A database object
     *
     * @since    1.6
     */
    public function getTable($type = 'Poll', $prefix = 'PollsTable', $config = array()) {
        return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * Method to get the record form.
     *
     * @param   array    $data      An optional array of data for the form to interogate.
     * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
     *
     * @return  JForm  A JForm object on success, false on failure
     *
     * @since    1.6
     */
    public function getForm($data = array(), $loadData = true) {
        // Initialise variables.
        $app = JFactory::getApplication();

        // Get the form.
        $form = $this->loadForm(
                'com_polls.poll', 'poll', array('control' => 'jform',
            'load_data' => $loadData
                )
        );

        if (empty($form)) {
            return false;
        }

        return $form;
    }

    /**
     * Method to get the data that should be injected in the form.
     *
     * @return   mixed  The data for the form.
     *
     * @since    1.6
     */
    protected function loadFormData() {
        // Check the session for previously entered form data.
        $data = JFactory::getApplication()->getUserState('com_polls.edit.poll.data', array());

        if (empty($data)) {
            if ($this->item === null) {
                $this->item = $this->getItem();
            }

            $data = $this->item;
        }

        return $data;
    }

    /**
     * Method to get a single record.
     *
     * @param   integer  $pk  The id of the primary key.
     *
     * @return  mixed    Object on success, false on failure.
     *
     * @since    1.6
     */
    public function getItem($pk = null) {
        if ($item = parent::getItem($pk)) {
            // Do any procesing on fields here if needed
            $registry = new JRegistry;
            $registry->loadString($item->catid);
            $item->catid = $registry->toArray();
            if (!empty($item->id)) {
                $item->tags = new JHelperTags;
                $item->tags->getTagIds($item->id, 'com_polls.poll');
            }
        }
        return $item;
    }

    public function getPoll($pk = null) {
        $pk = (!empty($pk)) ? $pk : (int) $this->getState($this->getName() . '.id');
        $table = $this->getTable();

        if (isset($pk[0]) && !empty($pk[0])) {
            $pk = $pk[0];
        }

        if ($pk > 0) {
            // Attempt to load the row.
            $return = $table->load($pk);

            // Check for a table object error.
            if ($return === false && $table->getError()) {
                $this->setError($table->getError());

                return false;
            }
        }

        // Convert to the JObject before adding other data.
        $properties = $table->getProperties(1);
        $item = JArrayHelper::toObject($properties, 'JObject');

        if (property_exists($item, 'params')) {
            $registry = new Registry;
            $registry->loadString($item->params);
            $item->params = $registry->toArray();
        }


        return $item;
    }

    /**
     * Method to duplicate an Poll
     *
     * @param   array  &$pks  An array of primary key IDs.
     *
     * @return  boolean  True if successful.
     *
     * @throws  Exception
     */
    public function duplicate(&$pks) {
        $user = JFactory::getUser();

        // Access checks.
        if (!$user->authorise('core.create', 'com_polls')) {
            throw new Exception(JText::_('JERROR_CORE_CREATE_NOT_PERMITTED'));
        }

        $dispatcher = JEventDispatcher::getInstance();
        $context = $this->option . '.' . $this->name;

        // Include the plugins for the save events.
        JPluginHelper::importPlugin($this->events_map['save']);

        $table = $this->getTable();

        foreach ($pks as $pk) {
            if ($table->load($pk, true)) {
                // Reset the id to create a new record.
                $table->id = 0;

                if (!$table->check()) {
                    throw new Exception($table->getError());
                }


                // Trigger the before save event.
                $result = $dispatcher->trigger($this->event_before_save, array($context, &$table, true));

                if (in_array(false, $result, true) || !$table->store()) {
                    throw new Exception($table->getError());
                }

                // Trigger the after save event.
                $dispatcher->trigger($this->event_after_save, array($context, &$table, true));
            } else {
                throw new Exception($table->getError());
            }
        }

        // Clean cache
        $this->cleanCache();

        return true;
    }

    public function save($data) {


        $app = JFactory::getApplication();
        $input = $app->input;
        $answers = $input->getVar('answers');
        $images = JRequest::getVar('image1');
        $sort_order = $input->get('sort_order');
        $idx = $input->get('idx');


        if (isset($data['catid']) && is_array($data['catid'])) {
            $catid = $data['catid'];
            $registry = new JRegistry;
            $registry->loadArray($data['catid']);
            $data['catid'] = (string) $registry;
        }

        $db = JFactory::getDBO();
        if (parent::save($data)) {

            if ($data['id']) {

                //delete if category_rel
                
                $query = 'DELETE FROM #__category_rel WHERE poll_id = ' . $data['id'] . '';
                $db->setQuery($query);
                $db->execute();
                foreach ($catid as $ct) {
                    $query = 'INSERT INTO #__category_rel (poll_id,cat_id)VALUES(' . $data['id'] . ',' . $ct . ')';
                    $db->setQuery($query);
                    $db->execute();
                }
            }

            $adata = array();
            $i = 0;
            if ($answers) {
                if ($data['id']){
                    $pollid = JRequest::getInt('id', 0);
                }else{
                    $query = 'SELECT id FROM #__polls WHERE state = 1 ORDER BY id DESC';
                    $db->setQuery($query);
                    $p = $db->loadObject();
                    if (isset($p->id)){
                        $pollid = $p->id;
                    }
                }
                foreach ($answers as $item) {

                    $adata[] = array(
                        'id' => isset($idx[$i]) && $idx[$i] ? $idx[$i] : 0,
                        'title' => $item,
                        'poll_id' => $pollid,
                        'ordering' => isset($sort_order[$i]) && $sort_order[$i] ? $sort_order[$i] : 0,
                        'image1' => isset($images[$i]) && $images[$i] ? $images[$i] : ''
                    );
                    $i++;
                }
            }

            if ($adata) {
                foreach ($adata as $row) {


                    $this->addAnswers($row);
                }
            }

            return true;
        }
        return false;
    }

    /**
     * Prepare and sanitise the table prior to saving.
     *
     * @param   JTable  $table  Table Object
     *
     * @return void
     *
     * @since    1.6
     */
    protected function prepareTable($table) {
        jimport('joomla.filter.output');

        if (empty($table->id)) {
            // Set ordering to the last item if not set
            if (@$table->ordering === '') {
                $db = JFactory::getDbo();
                $db->setQuery('SELECT MAX(ordering) FROM #__polls');
                $max = $db->loadResult();
                $table->ordering = $max + 1;
            }
        }
    }

    public function getAnswers() {
        $id = JRequest::getInt('id', 0);
        if ($id) {
            $db = JFactory::getDBO();

            $query = 'SELECT * FROM #__polls_answers WHERE state = 1 AND poll_id = ' . $id . ' ORDER BY ordering ASC';
            $db->setQuery($query);
            $result = $db->loadObjectList();

            return $result;
        }
    }

    public function addAnswers($data) {

        $db = JFactory::getDBO();
        if (!$data['title']) {
            return false;
        }
        if (isset($data['id']) && $data['id'] && isset($data['poll_id'])) {
            $sql = 'SELECT * FROM #__polls_answers WHERE poll_id = ' . (int) $data['poll_id'] . ' AND id = ' . (int) $data['id'] . ' AND state = 1 ';
            $db->setQuery($sql);
            $answer = $db->LoadObject();

            if (isset($answer->id)) {
                $image1 = $data['image1'] ? $data['image1'] : $answer->image1;
                $update = 'UPDATE #__polls_answers SET ordering = ' . (int) $data['ordering'] . ', title = ' . $db->quote($data['title']) . ', image1 = ' . $db->quote($image1) . ' WHERE poll_id = ' . (int) $data['poll_id'] . ' AND id = ' . $data['id'] . ' ';
                $db->setQuery($update);
                $db->execute();
            }
        }
        if (isset($data['id']) && $data['id'] == 0) {
            $query = 'INSERT INTO #__polls_answers (ordering,poll_id,title,image1,state,language)VALUES(' . (int) $data['ordering'] . ',' . $data['poll_id'] . ',' . $db->quote($data['title']) . ',' . $db->quote($image1) . ',1,"*")';
            $db->setQuery($query);
            $db->execute();
        }
    }

}
